import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRestService } from '../rest-services/auth.rest.service';
import { SecurityRestService } from '../rest-services/security.rest.service';
import { SecurityGroupsRestService } from '../rest-services/security.groups.rest.service';
import { UserRestService } from '../rest-services/user.rest.service';
import { MenuRestService } from '../rest-services/menu.rest.service';
import { TenantRestService } from '../rest-services/tenant.rest.service';
import { LookupRestService } from '../rest-services/lookup.rest.service';

@NgModule({
  imports: [
    AuthRestService,
    SecurityRestService,
    SecurityGroupsRestService,
    UserRestService,
    MenuRestService,
    TenantRestService,
    LookupRestService
  ],
  exports: [
    AuthRestService,
    SecurityRestService,
    SecurityGroupsRestService,
    UserRestService,
    MenuRestService,
    TenantRestService,
    LookupRestService
  ]
})
export class RestServiceModule {
  constructor(
    public auth: AuthRestService,
    public security: SecurityRestService,
    public securityGroups: SecurityGroupsRestService,
    public users: UserRestService,
    public menu: MenuRestService,
    public tenant: TenantRestService,
    public lookup: LookupRestService
  ){}
}
