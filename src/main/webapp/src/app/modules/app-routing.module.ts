import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { PasswordResetComponent } from '../components/password-reset/password.reset.component';
import { SecurityGroupsComponent } from '../components/admin/security-groups/security.groups.component';
import { CreateSecurityGroupComponent } from '../components/admin/security-groups/create.security.group.component';
import { EditSecurityGroupComponent } from '../components/admin/security-groups/edit.security.group.component';
import { MenusComponent } from '../components/admin/menus/menus.component';
import { CreateMenuComponent } from '../components/admin/menus/create.menu.component';
import { EditMenuComponent } from '../components/admin/menus/edit.menu.component';
import { CreateMenuItemComponent } from '../components/admin/menus/create.menu.item.component';
import { UserListComponent } from '../components/form-list-views/user-list/user.list.component';
import { UserCreateComponent } from '../components/forms/user/user.create.component';
import { TenantListComponent } from '../components/form-list-views/tenant-list/tenant.list.component';
import { TenantCreateComponent } from '../components/forms/tenant/tenant.create.component';
import { UserUpdateComponent } from '../components/form-views/user/user.update.component';
import { TenantUpdateComponent } from '../components/form-views/tenant/tenant.update.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'reset-password', component: PasswordResetComponent },
  { path: 'reset-password/:token', component: PasswordResetComponent },
  { path: 'user-list', component: UserListComponent },
  { path: 'user-create', component: UserCreateComponent },
  { path: 'user/:id/edit', component: UserUpdateComponent },
  { path: 'tenant-list', component: TenantListComponent },
  { path: 'tenant-create', component: TenantCreateComponent },
  { path: 'tenant/:id/edit', component: TenantUpdateComponent },
  { path: 'admin', children: [
    { path: 'security-groups', component: SecurityGroupsComponent },
    { path: 'security-groups/create', component: CreateSecurityGroupComponent },
    { path: 'security-groups/edit/:id', component: EditSecurityGroupComponent },
    { path: 'menus', component: MenusComponent },
    { path: 'menus/create', component: CreateMenuComponent },
    { path: 'menus/edit/:id', component: EditMenuComponent },
    { path: 'menus/:id/item/:parent/create', component: CreateMenuItemComponent },
    { path: 'menus/:id/item/create', component: CreateMenuItemComponent },
    { path: 'menus/item/edit/:id', component: CreateMenuItemComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
