import { RestServiceModule } from './rest.service.module';

describe('Rest.ServiceModule', () => {
  let restServiceModule: RestServiceModule;

  beforeEach(() => {
    restServiceModule = new RestServiceModule();
  });

  it('should create an instance', () => {
    expect(restServiceModule).toBeTruthy();
  });
});
