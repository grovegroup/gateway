import { NgModule } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { SecurityService } from '../services/security.service';
import { SecurityGroupsService } from '../services/security.groups.service';
import { UserService } from '../services/user.service';
import { MenuService } from '../services/menu.service';
import { UserPreferenceService } from '../services/user.preference.service';
import { TenantService } from '../services/tenant.service';
import { LookupService } from '../services/lookup.service';

@NgModule({
  imports: [
    AuthService,
    SecurityService,
    SecurityGroupsService,
    UserService,
    MenuService,
    UserPreferenceService,
    TenantService,
    LookupService
  ],
  exports:[
    AuthService,
    SecurityService,
    SecurityGroupsService,
    UserService,
    MenuService,
    UserPreferenceService,
    TenantService,
    LookupService
  ]
})
export class ServicesModule {
  constructor(
    public auth:AuthService,
    public security:SecurityService,
    public securityGroups: SecurityGroupsService,
    public users: UserService,
    public menu: MenuService,
    public userPreference: UserPreferenceService,
    public tenant: TenantService,
    public lookup: LookupService
  ){}
}
