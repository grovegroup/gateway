import { ServicesModule } from './services.module';

describe('Services.ModuleModule', () => {
  let servicesModuleModule: ServicesModule;

  beforeEach(() => {
    servicesModuleModule = new ServicesModule();
  });

  it('should create an instance', () => {
    expect(servicesModuleModule).toBeTruthy();
  });
});
