import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesModule } from 'src/app/modules/services.module';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SecurityGroupModel } from 'src/app/models/security.group.model';
import { TenantModel } from 'src/app/models/tenant.model';

@Component({
  selector: 'app-user-create',
  templateUrl: './user.create.component.html',
  styleUrls: ['./user.create.component.scss'],
})
export class UserCreateComponent implements OnInit {

  userForm: FormGroup;
  tenants = new Array<TenantModel>();
  securityGroups = new Array<SecurityGroupModel>();

  constructor(
    private _formBuilder: FormBuilder,
      private services: ServicesModule,
      private router : Router,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (!this.services.auth.isAuthenticated()) {
      return this.router.navigate(['login']);
    }

    this.createForm();
    this.getTenants();
    this.getSecurityGroups();
  }

  getErrorMessage(field: string) {
    return this.userForm.get(field).hasError('required') ? 'You must enter a value' : '';
  }

  createForm(): void {
    this.userForm = this._formBuilder.group({
      emailAddress: new FormControl('', Validators.compose([
        Validators.required])),
      firstName: new FormControl('', Validators.compose([
        Validators.required])),
      lastName: new FormControl('', Validators.compose([
        Validators.required])),
      password: new FormControl('', Validators.compose([
        Validators.required])),
      tenantId: new FormControl('', Validators.compose([
        Validators.required])),
      securityGroupId: new FormControl('', Validators.compose([
        Validators.required])),
      securityGroupIds: new FormArray([
        new FormControl('')
      ])
    });
  }

  onSubmit(form: any) {
    console.log('Not yet implemented');
  }

  save() {

    (<FormArray>this.userForm.get('securityGroupIds')).removeAt(0);
    (<FormArray>this.userForm.get('securityGroupIds')).push(new FormControl(this.userForm.get('securityGroupId').value));

    this.services.users.createUser(this.userForm.value).then (
        res => {
          this.showSnack('User successfully saved.');
          this.router.navigate(['user-list']);
        },
        err => {
          this.showSnack(err);
        }
    );
  }

  getTenants(){
    this.services.tenant.getAllTenants()
      .then(
        res => {
          console.log(res);
          this.tenants = res;
        },
        err => {
          console.log();
        }
      );
  }

  getSecurityGroups(){
    this.services.securityGroups.getSecurityGroups()
      .then(
        res => {
          console.log(res);
          this.securityGroups = res;
        },
        err => {
          console.log();
        }
      );
  }

  onViewUsers() {
    this.router.navigate(['user-list']);
  }

  showSnack(message: string) {
    this.snackBar.open(message, "OK", { duration: 5000 });
  }
}
