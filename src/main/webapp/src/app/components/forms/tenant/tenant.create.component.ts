import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesModule } from 'src/app/modules/services.module';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { LookupResponseDTO } from 'src/app/dto/response/lookup.response.dto';

@Component({
  selector: 'app-tenant-create',
  templateUrl: './tenant.create.component.html',
  styleUrls: ['./tenant.create.component.scss'],
})
export class TenantCreateComponent implements OnInit {

  tenantForm: FormGroup;
  databases = new Array<LookupResponseDTO>();

  constructor(
    private _formBuilder: FormBuilder,
      private services: ServicesModule,
      private router : Router,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (!this.services.auth.isAuthenticated()) {
      return this.router.navigate(['login']);
    }

    this.createForm();
    this.getDatabases();
  }

  getErrorMessage(field: string) {
    return this.tenantForm.get(field).hasError('required') ? 'You must enter a value' : '';
  }

  createForm(): void {
    this.tenantForm = this._formBuilder.group({
      tenantName: new FormControl('', Validators.compose([
        Validators.required])),
      databaseType: new FormControl('', Validators.compose([
        Validators.required]))
    });
  }

  save() {
    console.log(this.tenantForm.value);
    this.services.tenant.createTenant(this.tenantForm.value).then (
        res => {
          this.showSnack('Tenant successfully saved.');
          this.router.navigate(['tenant-list']);
        },
        err => {
          this.showSnack(err);
        }
    );
  }

  getDatabases(){
    this.services.lookup.getLookupOptionsByType('DATABASE_TYPE')
      .then(
        res => {
          console.log(res);
          this.databases = res;
        },
        err => {
          console.log();
        }
      );
  }

  onViewTenants() {
    this.router.navigate(['tenant-list']);
  }

  showSnack(message: string) {
    this.snackBar.open(message, "OK", { duration: 5000 });
  }
}
