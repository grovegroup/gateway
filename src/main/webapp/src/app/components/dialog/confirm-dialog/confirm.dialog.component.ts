import { Component, OnInit, Inject, AfterContentInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HeaderMessageDTO } from 'src/app/dto/header.message.dto';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm.dialog.component.html',
  styleUrls: ['./confirm.dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit, AfterContentInit {
  ngAfterContentInit(): void {
    let focusElement = document.getElementById('cancelDel');
    focusElement.focus();
  }

  constructor(private dialogref: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: HeaderMessageDTO) { }

  ngOnInit() {}

  onConfirm() {
    this.dialogref.close(true);
  }

  onCancel() {
    this.dialogref.close(false);
  }
}
