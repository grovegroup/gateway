import { Component, OnInit } from "@angular/core";
import { ServicesModule } from "../../../modules/services.module";
import { SecurityGroupModel } from "../../../models/security.group.model";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar, MatDialog } from "@angular/material";
import { ConfirmationDialogComponent } from "../../../mellow-controls/confirmation.dialog.component";

@Component({
    selector: 'edit-security-group',
    templateUrl: './edit.security.group.component.html',
    styleUrls: ['./edit.security.group.component.css']
})
export class EditSecurityGroupComponent implements OnInit {

    model: SecurityGroupModel = new SecurityGroupModel();
    errors: string[];
    busy: boolean;
    id: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private services: ServicesModule,
        private snackBar: MatSnackBar,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');
        this.busy = true;
        this.services.securityGroups.getSecurityGroup(this.id)
            .then((res) => {
                this.model = res;
            })
            .then(() => this.busy = false);
    }

    OnSubmit() {
        this.busy = true;
        this.errors = null;
        this.services.securityGroups.updateSecurityGroup(this.model)
            .then(
                (res) => {
                    if (res.securityGroupId > '')
                        this.router.navigate(['admin/security-groups']);
                    else
                        this.toast('Oops! Something went wrong, please try again.');
                },
                (err) => {
                    console.log(err);
                    if (err.length > 1) {
                        this.errors = err;
                    } else {
                        this.toast(err);
                    }
                }
            ).then(() => this.busy = false);
    }

    Delete() {
        this.dialog.open(ConfirmationDialogComponent, {
            width: '400px',
            data: 'Are you sure you want to delete this security group?'
        }).afterClosed().subscribe(result => {
            if (result) {
                this.busy = true;
                this.errors = null;
                this.services.securityGroups.deleteSecurityGroup(this.model.securityGroupId)
                    .then(
                        (res) => {
                            if (res.securityGroupId > '')
                                this.router.navigate(['admin/security-groups']);
                            else
                                this.toast('Oops! Something went wrong, please try again.');
                        },
                        (err) => {
                            console.log(err);
                            if (err.length > 1) {
                                this.errors = err;
                            } else {
                                this.toast(err);
                            }
                        }
                    ).then(() => this.busy = false);
            }
        });
    }

    toast(message: string) {
        this.snackBar.open(message, "OK", {
            duration: 5000,
            verticalPosition: 'top'
        });
    }
}