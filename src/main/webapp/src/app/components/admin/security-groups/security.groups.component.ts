import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesModule } from '../../../modules/services.module';
import { SecurityGroupModel } from '../../../models/security.group.model';
import { MatPaginator, PageEvent, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector: 'security-groups',
    templateUrl: './security.groups.component.html',
    styleUrls: ['./security.groups.component.css']
})
export class SecurityGroupsComponent implements OnInit {
    displayedColumns = ['securityGroupId', 'groupName'];
    pageSizeOptions = [5, 10, 15];
    dataSource: MatTableDataSource<SecurityGroupModel>;
    busy: boolean = false;
    resultsLength: number;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private services: ServicesModule,
        private router: Router
    ) { }

    ngOnInit() {
        this.getSecurityGroups(0, 5);
    }

    getSecurityGroups(page: number, size: number) {
        this.busy = true;
        this.services.securityGroups.searchSecurityGroups(page, size)
            .then(
                (res) => {
                    this.dataSource = new MatTableDataSource(res.content);
                    this.dataSource.sort = this.sort;
                    this.dataSource.paginator = this.paginator;
                    this.resultsLength = res.totalElements;
                },
                (err) => {
                    console.log(err);
                }
            ).then(() => this.busy = false);
    }

    rowClick(securityGroup) {
        this.router.navigate(['admin/security-groups/edit/' + securityGroup.securityGroupId]);
    }

    pageChange(pageEvent: PageEvent) {
        this.getSecurityGroups(pageEvent.pageIndex, pageEvent.pageSize);
    }
}
