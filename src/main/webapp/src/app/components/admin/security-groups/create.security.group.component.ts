import { Component } from "@angular/core";
import { ServicesModule } from "../../../modules/services.module";
import { SecurityGroupModel } from "../../../models/security.group.model";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";

@Component({
    selector: 'create-security-group',
    templateUrl: './create.security.group.component.html',
    styleUrls: ['./create.security.group.component.css']
})
export class CreateSecurityGroupComponent {

    model: SecurityGroupModel = new SecurityGroupModel();
    errors: string[];
    busy: boolean;

    constructor(
        private router: Router,
        private services: ServicesModule,
        private snackBar: MatSnackBar
    ){}

    OnSubmit(){
        this.busy = true;
        this.errors = null;
        this.services.securityGroups.createSecurityGroups(this.model)
        .then(
            (res) => {
                if(res.securityGroupId > '')
                    this.router.navigate(['admin/security-groups']);
                else
                    this.toast('Oops! Something went wrong, please try again.');
            },
            (err) => {
                console.log(err);
                if(err.length > 1){
                    this.errors = err;
                }else{
                    this.toast(err);
                }
            }
        ).then(() => this.busy = false);
    }

    toast(message: string){
        this.snackBar.open(message, "OK", {
            duration: 5000,
            verticalPosition: 'top'
        });
    }
}