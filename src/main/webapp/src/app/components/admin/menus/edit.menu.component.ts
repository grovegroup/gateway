import { Component, OnInit } from "@angular/core";
import { ServicesModule } from "../../../modules/services.module";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { MenuModel } from "../../../models/menu.model";

@Component({
    selector: 'edit-menu',
    templateUrl: './edit.menu.component.html',
    styleUrls: ['./edit.menu.component.css']
})
export class EditMenuComponent implements OnInit {

    model: MenuModel = new MenuModel();
    errors: string[];
    busy: boolean;
    id:string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private services: ServicesModule,
        private snackBar: MatSnackBar
    ){}

    ngOnInit(){
        this.id = this.route.snapshot.paramMap.get('id');
        this.busy = true;
        this.services.menu.getMenu(this.id)
        .then((res) => {
            this.model = res; 
        })
        .then(() => this.busy = false);
    }

    OnSubmit(){
        this.busy = true;
        this.errors = null;
        this.services.menu.updateMenu(this.model)
        .then(
            (res) => {
                if(res.menuId > '')
                    this.router.navigate(['admin/menus']);
                else
                    this.toast('Oops! Something went wrong, please try again.');
            },
            (err) => {
                console.log(err);
                if(err.length > 1){
                    this.errors = err;
                }else{
                    this.toast(err);
                }
            }
        ).then(() => this.busy = false);
    }

    toast(message: string){
        this.snackBar.open(message, "OK", {
            duration: 5000,
            verticalPosition: 'top'
        });
    }
}