import { Component, OnInit } from "@angular/core";
import { ServicesModule } from "../../../modules/services.module";
import { MenuModel } from "../../../models/menu.model";
import { MatSnackBar, MatDialog } from "@angular/material";
import { ConfirmationDialogComponent } from "../../../mellow-controls/confirmation.dialog.component";

@Component({
    selector: 'menus',
    templateUrl: './menus.component.html',
    styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {

    busy: boolean = false;
    menus: MenuModel[];
    errors: string[];
    constructor(
        private services: ServicesModule,
        private snackBar: MatSnackBar,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.getMenus();
    }

    getMenus() {
        this.busy = true;
        this.services.menu.getMenus()
            .then(
                (res) => {
                    console.log(res);
                    this.menus = res;
                },
                (err) => {
                    if (err.length > 1) {
                        this.errors = err;
                    } else {
                        this.toast(err);
                    }
                }
            ).then(() => this.busy = false);
    }

    deleteMenu(menuId: string) {
        this.dialog.open(ConfirmationDialogComponent, {
            width: '400px',
            data: 'Are you sure you want to delete this menu?'
        }).afterClosed().subscribe(result => {
            if (result) {
                this.busy = true;
                this.services.menu.deleteMenu(menuId)
                    .then(
                        (res) => {
                            this.getMenus();
                        },
                        (err) => {
                            if (err.length > 1) {
                                this.errors = err;
                            } else {
                                this.toast(err);
                            }
                            this.busy = false;
                        }
                    ).then(() => this.busy = false);
            }
        });
    }

    deleteMenuItem(menuItemId: string) {
        this.dialog.open(ConfirmationDialogComponent, {
            width: '400px',
            data: 'Are you sure you want to delete this menu item?'
        }).afterClosed().subscribe(result => {
            if (result) {
                this.busy = true;
                this.services.menu.deleteMenuItem(menuItemId)
                    .then(
                        (res) => {
                            this.getMenus();
                        },
                        (err) => {
                            if (err.length > 1) {
                                this.errors = err;
                            } else {
                                this.toast(err);
                            }
                            this.busy = false;
                        }
                    ).then(() => this.busy = false);
            }
        });
    }

    toast(message: string) {
        this.snackBar.open(message, "OK", {
            duration: 5000,
            verticalPosition: 'top'
        });
    }
}