import { Component, OnInit } from "@angular/core";
import { ServicesModule } from "../../../modules/services.module";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { MenuItemModel } from "../../../models/menu.item.model";

@Component({
    selector: 'create-menu-item',
    templateUrl: './create.menu.item.component.html',
    styleUrls: ['./create.menu.item.component.css']
})
export class CreateMenuItemComponent implements OnInit {

    model: MenuItemModel = new MenuItemModel();
    errors: string[];
    busy: boolean;
    menuId:string;
    parentId:string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private services: ServicesModule,
        private snackBar: MatSnackBar
    ){}

    ngOnInit(){
        this.menuId = this.route.snapshot.paramMap.get('id');
        this.parentId = this.route.snapshot.paramMap.get('parent');
        this.model.menuId = this.menuId;
        this.model.parentId = this.parentId;
    }

    OnSubmit(){
        this.busy = true;
        this.errors = null;
        this.services.menu.createMenuItem(this.model)
        .then(
            (res) => {
                if(res.menuId > '')
                    this.router.navigate(['admin/menus']);
                else
                    this.toast('Oops! Something went wrong, please try again.');
            },
            (err) => {
                console.log(err);
                if(err.length > 1){
                    this.errors = err;
                }else{
                    this.toast(err);
                }
            }
        ).then(() => this.busy = false);
    }

    toast(message: string){
        this.snackBar.open(message, "OK", {
            duration: 5000,
            verticalPosition: 'top'
        });
    }
}