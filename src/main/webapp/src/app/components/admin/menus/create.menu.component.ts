import { Component } from "@angular/core";
import { ServicesModule } from "../../../modules/services.module";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { MenuModel } from "../../../models/menu.model";

@Component({
    selector: 'create-menu',
    templateUrl: './create.menu.component.html',
    styleUrls: ['./create.menu.component.css']
})
export class CreateMenuComponent {

    model: MenuModel = new MenuModel();
    errors: string[];
    busy: boolean;

    constructor(
        private router: Router,
        private services: ServicesModule,
        private snackBar: MatSnackBar
    ){}

    OnSubmit(){
        this.busy = true;
        this.errors = null;
        this.services.menu.createMenu(this.model)
        .then(
            (res) => {
                if(res.menuId > '')
                    this.router.navigate(['admin/menus']);
                else
                    this.toast('Oops! Something went wrong, please try again.');
            },
            (err) => {
                console.log(err);
                if(err.length > 1){
                    this.errors = err;
                }else{
                    this.toast(err);
                }
            }
        ).then(() => this.busy = false);
    }

    toast(message: string){
        this.snackBar.open(message, "OK", {
            duration: 5000,
            verticalPosition: 'top'
        });
    }
}