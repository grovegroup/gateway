import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesModule } from 'src/app/modules/services.module';
import { interval, Subscription } from 'rxjs';

export interface Notification {
  notificationId: string,
  entityId: string,
  entityType: string,
  type: string,
  description: string,
  isRead: boolean,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  subscription: Subscription;
  intervalId: number;
  ngOnInit(): void {

  }

  title = 'Fleet Control';
  darkTheme = false;
  authed: boolean = false;
  opened: boolean = false;
  events: string[] = [];

  constructor(
    private route: Router,
    private services: ServicesModule
  ) {

  }

  logout() {
    this.services.auth.SetAuthenticated(false);
    this.services.auth.SetToken('');
    this.route.navigate(['login']);
  }

  isAuthenticated() {
    this.authed = this.services.auth.isAuthenticated();
    return this.authed;
  }

  closeNavbar() {
    this.opened = false;
  }
}
