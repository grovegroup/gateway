import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PasswordResetModel } from '../../models/password.reset.model';
import { ServicesModule } from '../../modules/services.module';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'password-reset',
  templateUrl: './password.reset.component.html',
  styleUrls: ['./password.reset.component.css']
})
export class PasswordResetComponent implements OnInit {
    hide = true;
    model: PasswordResetModel = new PasswordResetModel(null, null, null);
    busy:boolean = false;
    errors: string[];

    constructor(
        private services: ServicesModule,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar
    ){}

    ngOnInit(): void {
        this.getResetToken();
    }
      
    getResetToken(): void {
        this.model.token = this.route.snapshot.paramMap.get('token');
    }

    OnSubmit(){
        this.busy = true;
        this.errors = null;
        if(this.model.token){
            //update password
            this.resetPassword();
        }else{
            //request reset
            this.requestPasswordReset();
        }
    }

    requestPasswordReset(){
        this.services.security.requestPasswordReset(this.model)
        .then(
            result => {
                console.log(result);
            },
            (err) =>{
                console.log(err);
                if(err.length > 1){
                    this.errors = err;
                }else{
                    this.snackBar.open(err, "OK", {
                    duration: 5000,
                    verticalPosition: 'top'
                    });
                }
            }
        ).then(() => this.busy = false);
    }

    resetPassword(){
        this.services.security.resetPassword(this.model)
        .then(
            result =>{
                console.log(result);
            },
            (err) =>{
                console.log(err);
                if(err.length > 1){
                    this.errors = err;
                }else{
                    this.snackBar.open(err, "OK", {
                    duration: 5000,
                    verticalPosition: 'top'
                    });
                }
            }
        ).then(() => this.busy = false);
    }
}
