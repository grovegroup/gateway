import { Component } from '@angular/core';
import { RegisterModel } from '../../models/register.model';
import { MatSnackBar } from '@angular/material';
import { ServicesModule } from '../../modules/services.module';

@Component({
  selector: 'login-component',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  hide = true;
  busy: boolean = false;
  regModel: RegisterModel = new RegisterModel();
  errors: string[];
  currentYear = new Date().getFullYear();

  constructor(
    private services: ServicesModule,
    private snackBar: MatSnackBar
  ) { }

  OnSubmit() {
    this.busy = true;
    this.services.security.register(this.regModel)
      .then(token => {
        console.log(token.tokenType);
        console.log(token.authToken);
      },
        err => {
          console.log(err);
          if (err.length > 1) {
            this.errors = err;
          } else {
            this.snackBar.open(err, "OK", {
              duration: 5000,
              verticalPosition: 'top'
            });
          }
        }).then(() => this.busy = false);
  }
}
