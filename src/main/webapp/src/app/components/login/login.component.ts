import { Component, OnInit } from '@angular/core';
import { AuthModel } from '../../models/auth.model';
import { ServicesModule } from '../../modules/services.module';
import { MatSnackBar, MatBottomSheet } from '@angular/material';
import { Router } from '@angular/router';
import { BottomSheetComponent } from '../../mellow-controls/bottom-sheet/bottom.sheet.component';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  authModel: AuthModel = new AuthModel('', '');
  busy: boolean = false;
  errors: string[];
  currentYear = new Date().getFullYear();

  constructor(
    private services: ServicesModule,
    private snackBar: MatSnackBar,
    private router: Router,
    private bottomSheet: MatBottomSheet

  ) { }

  ngOnInit() {
    if(this.services.auth.isAuthenticated()){
      this.router.navigate(['user-list']);
    }
  }

  OnSubmit() {
    this.busy = true;
    this.errors = null;

    this.services.auth.SetToken('');
    let response = this.services.security.authenticate(this.authModel);
    response.then(token => {
      this.services.auth.SetToken(token.authToken);
      this.services.auth.SetAuthenticated(true);
      this.getMe();
    },
      err => {
        this.services.auth.SetAuthenticated(false);
        if (err.length > 1) {
          this.errors = err;
        } else {
          this.snackBar.open(err, "OK", {
            duration: 5000,
            verticalPosition: 'top'
          });
        }
      }).then(() => this.busy = false);
  }

  getMe(){
      this.services.users.getMe().then (
        res => {
          let user = res;
          this.services.auth.SetProfile(user);
          this.router.navigate(['user-list']);
        },
        err => {

        }
    );
  }

  openBottomSheet(): void {
    this.bottomSheet.open(BottomSheetComponent);
  }
}
