import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, PageEvent, MatSnackBar } from '@angular/material';
import { ServicesModule } from 'src/app/modules/services.module';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { SearchRequestDTO } from 'src/app/models/search/search.request.dto';
import { TenantResponseDTO } from 'src/app/dto/response/tenant.response.dto';
import { ConfirmDialogComponent } from '../../dialog/confirm-dialog/confirm.dialog.component';
import { HeaderMessageDTO } from 'src/app/dto/header.message.dto';

@Component({
    selector: 'app-tenant-list',
    templateUrl: './tenant.list.component.html',
    styleUrls: ['./tenant.list.component.scss']
})
export class TenantListComponent implements OnInit {

    //Fields
    busy: boolean = false;
    transactionNumber = 0;
    tooltipPosition = 'above';

    //Table settings
    displayedColumns = ['tenantName', 'tenantEnvironmentName', 'users', 'actions'];
    tenant: TenantResponseDTO;

    //View Elements
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    // Pagination component
    itemsPerPage = 10;
    selected = 10;
    pageError = false;
    collection = { count: 60, data: [] };
    itemsPerPageOptions = [5,10,15,25,50,100];
    config = {
        id: 'custom',
        itemsPerPage: this.itemsPerPage,
        currentPage: 0,
        totalItems: this.collection.count
    };
    num = 0;
    isRecordsAvailable: boolean;
    noRecord: boolean;
    dataSourceArray: TenantResponseDTO[];

    /**
    * @param services contains the services that connect to the rest services
    * @param dialog contains the services that connect to the rest services
    * @param router contains the application routing modules
    * @param snackBar allows viewing of the snackbar item on the page
    * @param ls service used to view static text on the application
    * @param dataService used to access subjects passed around the application
    */
    constructor(
        private services: ServicesModule,
        private dialog: MatDialog,
        private router: Router,
        private snackBar: MatSnackBar,
    ) { }

    isFiltered = false;
    errorMsg = ''
    isLoading: boolean;
    searchTenantsCtrl = new FormControl();
    indexSearch = 0;
    sizeSearch = 100;
    currentPage: number;
    searchText = '';

    ngOnInit() {
        //this.searchJobsCtrl.setValue(this.searchText);
        if (!this.services.auth.isAuthenticated()) {
            return this.router.navigate(['login']);
        }

        this.selected = +this.services.userPreference.GetItemsPerPage('itemsPerPageTenant');
        this.config.itemsPerPage = this.selected;

        this.searchTenantsCtrl.valueChanges.subscribe(value => {
          this.searchText = value;
          this.onPageChange(0);
        });

        this.onPageChange(0);
    }

    /**
     * Retrieves search results from the Auto filter.
     * @param pageIndex The requested page for paging results
     * @param pageSize The requested page number
     * @param searchText The string value entered by the user in the search box
     **/
    getRecords(pageIndex: number, pageSize: number, searchText: string) {
        this.searchText = searchText;
        let search = new SearchRequestDTO(this.searchText, pageIndex, pageSize, ++this.transactionNumber);
        return this.services.tenant.searchTenants(search)
    }

    /**
     * This method is be used to fetch the table data from the database
     * @param event The requested page for paging results
     **/
    onPageChange(event) {
        this.busy = true;
        let index = event;
        index = (index != 0) ? index - 1 : 0;

        let search = new SearchRequestDTO(this.searchText, index, this.config.itemsPerPage, ++this.transactionNumber);
        this.services.tenant.searchTenants(search).then(
            response => {
                this.busy = false;
                if(this.transactionNumber === response.transactionNumber){
                  this.config.totalItems = response['totalEntities'];
                  this.config.currentPage = event;
                  this.dataSourceArray = response.content;
                  this.isRecordsAvailable = this.dataSourceArray.length !== 0;
                }
            },
            error => {
                this.busy = false;
                this.showSnack(error); //TODO: Show meaningful error
            }
        );
    }

    /**
      * Routes the application to a new customer creation page.
      * @param
      **/
    addTenant() {
        this.router.navigate(['tenant-create']);
    }

    /**
    * Routes the application to a new customer creation page.
    * @param customerId This is the customer Id whose record is being deleted
    **/
    updateStatusTenant(tenant: TenantResponseDTO, status: boolean) {
        tenant.deleted = status;
        this.services.tenant.updateStatus(tenant.tenantId, status).then(
            () => {
                this.showSnack('Record successfully updated.');
            },
            err => {
                tenant.deleted = !status;
                this.showSnack(err);
            }
        );
    }

    openDialog(tenant: TenantResponseDTO, action: string) {

        let headerMessage = new HeaderMessageDTO();
        let status = true;
        switch(action){
            case 'D':
              status = true;
              headerMessage.header = 'Delete Tenant';
              headerMessage.message = 'Are you sure you want to delete ' + tenant.tenantName + '?';
              break;
            case 'R':
              status = false;
              headerMessage.header = 'Re-enable Tenant';
              headerMessage.message = 'Are you sure you want to re-enable ' + tenant.tenantName + '?';
              break;
        }

        let areaDialog = this.dialog.open(ConfirmDialogComponent, {
            width: "400px",
            disableClose: true,
            data: headerMessage,
            panelClass: 'confirm-dialog-container'
        });

        areaDialog.afterClosed().subscribe(result => {
            if (result != null) {
                if (result == true) {
                  this.updateStatusTenant(tenant, status);
                }
            }
        });
    }

    /**
    * Routes the application to a new customer edit page.
    * @param customerId This is the customer Id whose record is being edited
    **/
    viewTenant(tenant: any) {
        this.router.navigate(['tenant/' + tenant.tenantId + '/edit']);
    }

    /**
    * Resets the requested page number
    * @param itemsPerPage The number of records to be viewed at at time
    **/
    onSetItemsPerPage(itemsPerPage: number) {
        this.config.itemsPerPage = itemsPerPage;
        this.services.userPreference.SetUserPreference('itemsPerPageTenant', itemsPerPage);
        this.onPageChange(0);
    }

    /**
     * Snack bar
     * @param message String value to be shown in teh snack bar
     **/
    showSnack(message: string) {
        this.snackBar.open(message, "OK", { duration: 5000 });
    }
}
