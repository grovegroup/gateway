import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, PageEvent, MatSnackBar } from '@angular/material';
import { ServicesModule } from 'src/app/modules/services.module';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfileResponseDTO } from 'src/app/dto/response/user.profile.response.dto';
import { TenantModel } from 'src/app/models/tenant.model';
import { UserSearchRequestDTO } from 'src/app/models/search/user.search.request.dto';
import { ConfirmDialogComponent } from '../../dialog/confirm-dialog/confirm.dialog.component';
import { HeaderMessageDTO } from 'src/app/dto/header.message.dto';
import { tap, switchMap, finalize } from 'rxjs/operators';
import { UserModel } from 'src/app/models/user.model';

@Component({
    selector: 'app-user-list',
    templateUrl: './user.list.component.html',
    styleUrls: ['./user.list.component.scss']
})
export class UserListComponent implements OnInit {

    //Fields
    busy: boolean = false;
    transactionNumber = 0;
    tooltipPosition = 'above';
    tenants: Array<TenantModel>;
    tenantForm: FormGroup;
    selectedTenantId: string;

    //Table settings
    displayedColumns = ['firstName', 'lastName', 'emailAddress', 'tenantName', 'actions'];

    user: UserProfileResponseDTO;

    //View Elements
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    // Pagination component
    itemsPerPage = 10;
    selected = 10;
    pageError = false;
    collection = { count: 60, data: [] };
    itemsPerPageOptions = [5,10,15,25,50,100];
    config = {
        id: 'custom',
        itemsPerPage: this.itemsPerPage,
        currentPage: 0,
        totalItems: this.collection.count
    };
    num = 0;
    isRecordsAvailable: boolean;
    noRecord: boolean;
    dataSourceArray: UserProfileResponseDTO[];
    currentUser: UserModel;

    /**
    * @param services contains the services that connect to the rest services
    * @param dialog contains the services that connect to the rest services
    * @param router contains the application routing modules
    * @param snackBar allows viewing of the snackbar item on the page
    * @param ls service used to view static text on the application
    * @param dataService used to access subjects passed around the application
    */
    constructor(
        private services: ServicesModule,
        private dialog: MatDialog,
        private router: Router,
        private snackBar: MatSnackBar,
        private _formBuilder: FormBuilder
    ) { }

    isFiltered = false;
    errorMsg = ''
    isLoading: boolean;
    searchUsersCtrl = new FormControl();
    indexSearch = 0;
    sizeSearch = 100;
    currentPage: number;
    searchText = '';

    ngOnInit() {

        this.currentUser = this.services.auth.GetProfile();
        if (!this.services.auth.isAuthenticated()) {
            return this.router.navigate(['login']);
        }

        this.selected = +this.services.userPreference.GetItemsPerPage('itemsPerPageUser');
        this.config.itemsPerPage = this.selected;

        this.searchUsersCtrl.valueChanges.subscribe(value => {
          this.searchText = value;
          this.onPageChange(0);
        });

        this.createForm();
        this.getTenants();
    }

    createForm(): void {
      this.tenantForm = this._formBuilder.group({
        tenantId: new FormControl('', Validators.compose([
          Validators.required]))
      });
    }

    /**
     * This method is be used to fetch the table data from the database
     * @param event The requested page for paging results
     **/
    onPageChange(event):any {
        this.busy = true;
        let index = event;
        index = (index != 0) ? index - 1 : 0;

        let search = new UserSearchRequestDTO(this.selectedTenantId, index, this.config.itemsPerPage, ++this.transactionNumber, this.searchText);
        this.services.users.searchUsers(search).then(
            response => {
              this.busy = false;
                if(this.transactionNumber === response.transactionNumber){
                  this.config.totalItems = response['totalEntities'];
                  this.config.currentPage = event;
                  this.dataSourceArray = response.content;
                  this.isRecordsAvailable = this.dataSourceArray.length !== 0;
                }
            },
            error => {
                this.busy = false;
                this.showSnack(error); //TODO: Show meaningful error
            }
        )
    }

    tenantChanged(event: any){
      this.selectedTenantId = event.value;
      this.onPageChange(0);
    }

    getTenants(){
        this.services.tenant.getAllTenants().then(
          response => {
              this.tenants = response;
              this.tenantForm.patchValue({
                tenantId: this.tenants[0].tenantId
              });
              this.selectedTenantId = this.tenants[0].tenantId;
              this.onPageChange(0);
          },
          error => {
              this.showSnack(error); //TODO: Show meaningful error
          }
      )
    }

    /**
      * Routes the application to a new customer creation page.
      * @param
      **/
    addUser() {
        this.router.navigate(['user-create']);
    }

    /**
    * Routes the application to a new customer creation page.
    * @param customerId This is the customer Id whose record is being deleted
    **/
    updateStatus(user: UserProfileResponseDTO, status: boolean) {
        user.deleted = status;
        this.services.users.updateStatus(user.userId, status).then(
            () => {
                this.showSnack('Record successfully updated.');
            },
            err => {
                user.deleted = !status;
                this.showSnack(err);
            }
        )
    }

    /**
    * Routes the application to a new customer edit page.
    * @param customerId This is the customer Id whose record is being edited
    **/
    viewUser(user: any) {
        this.router.navigate(['user/' + user.userId + '/edit']);
    }

    /**
    * Resets the requested page number
    * @param itemsPerPage The number of records to be viewed at at time
    **/
    onSetItemsPerPage(itemsPerPage: number) {
        this.config.itemsPerPage = itemsPerPage;
        this.services.userPreference.SetUserPreference('itemsPerPageUser', itemsPerPage);
        this.onPageChange(0);
    }

    /**
     * Snack bar
     * @param message String value to be shown in teh snack bar
     **/
    showSnack(message: string) {
        this.snackBar.open(message, "OK", { duration: 5000 });
    }

    openDialog(user: UserProfileResponseDTO, action: string) {

        let headerMessage = new HeaderMessageDTO();
        let status = true;
        switch(action){
            case 'D':
              status = true;
              headerMessage.header = 'Delete User';
              headerMessage.message = 'Are you sure you want to delete ' + user.firstName + ' ' + user.lastName + '?';
              break;
            case 'R':
              status = false;
              headerMessage.header = 'Re-enable User';
              headerMessage.message = 'Are you sure you want to re-enable ' + user.firstName + ' ' + user.lastName + '?';
              break;
        }

        let areaDialog = this.dialog.open(ConfirmDialogComponent, {
            width: "400px",
            disableClose: true,
            data: headerMessage,
            panelClass: 'confirm-dialog-container'
        });

        areaDialog.afterClosed().subscribe(result => {
            if (result != null) {
                if (result == true) {
                  this.updateStatus(user, status);
                }
            }
        });
    }
}
