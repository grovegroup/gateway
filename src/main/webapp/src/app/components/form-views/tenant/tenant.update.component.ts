import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesModule } from 'src/app/modules/services.module';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { LookupResponseDTO } from 'src/app/dto/response/lookup.response.dto';
import { TenantResponseDTO } from 'src/app/dto/response/tenant.response.dto';

@Component({
  selector: 'app-tenant-update',
  templateUrl: './tenant.update.component.html',
  styleUrls: ['./tenant.update.component.scss'],
})
export class TenantUpdateComponent implements OnInit {

  tenantForm: FormGroup;
  tenantId: string;
  tenant: TenantResponseDTO;

  constructor(
    private _formBuilder: FormBuilder,
      private services: ServicesModule,
      private router : Router,
      private route: ActivatedRoute,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (!this.services.auth.isAuthenticated()) {
      return this.router.navigate(['login']);
    }

    this.route.params.subscribe(params => {
      this.tenantId = params['id'];
    });

    this.createForm();
    this.getTenant();
  }

  getErrorMessage(field: string) {
    return this.tenantForm.get(field).hasError('required') ? 'You must enter a value' : '';
  }

  createForm(): void {
    this.tenantForm = this._formBuilder.group({
      tenantName: new FormControl('', Validators.compose([
        Validators.required])),
      tenantId: new FormControl(this.tenantId, Validators.compose([
        Validators.required]))
    });
  }

  getTenant(){
      this.services.tenant.getTenantById(this.tenantId).then (
        res => {
          this.tenant = res;

          this.tenantForm.patchValue({
            tenantName: this.tenant.tenantName
          });
        },
        err => {
          this.showSnack(err);
        }
    );
  }

  save() {
    this.services.tenant.updateTenant(this.tenantForm.value).then (
        res => {
          this.showSnack('Tenant successfully saved.');
          this.router.navigate(['tenant-list']);
        },
        err => {
          this.showSnack(err);
        }
    );
  }

  onViewTenants() {
    this.router.navigate(['tenant-list']);
  }

  showSnack(message: string) {
    this.snackBar.open(message, "OK", { duration: 5000 });
  }
}
