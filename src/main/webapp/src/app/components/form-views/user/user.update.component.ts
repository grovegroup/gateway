import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesModule } from 'src/app/modules/services.module';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SecurityGroupModel } from 'src/app/models/security.group.model';
import { UserProfileResponseDTO } from 'src/app/dto/response/user.profile.response.dto';
import { TenantModel } from 'src/app/models/tenant.model';

@Component({
  selector: 'app-user-update',
  templateUrl: './user.update.component.html',
  styleUrls: ['./user.update.component.scss'],
})
export class UserUpdateComponent implements OnInit {

  userForm: FormGroup;
  user: UserProfileResponseDTO;
  securityGroups = new Array<SecurityGroupModel>();
  userId: string;

  constructor(
    private _formBuilder: FormBuilder,
      private services: ServicesModule,
      private router : Router,
      private route: ActivatedRoute,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (!this.services.auth.isAuthenticated()) {
      return this.router.navigate(['login']);
    }

    this.route.params.subscribe(params => {
      this.userId = params['id'];
    });

    this.createForm();
    this.getUser();
    this.getSecurityGroups();
  }

  getErrorMessage(field: string) {
    return this.userForm.get(field).hasError('required') ? 'You must enter a value' : '';
  }

  createForm(): void {
    this.userForm = this._formBuilder.group({
      userId: new FormControl(this.userId, Validators.compose([
        Validators.required])),
      emailAddress: new FormControl('', Validators.compose([
        Validators.required])),
      firstName: new FormControl('', Validators.compose([
        Validators.required])),
      lastName: new FormControl('', Validators.compose([
        Validators.required])),
      tenantName: new FormControl('', Validators.compose([
        Validators.required])),
      securityGroupId: new FormControl('', Validators.compose([
        Validators.required])),
      securityGroupIds: new FormArray([
        new FormControl('')
      ])
    });
  }

  getUser(){
      this.services.users.getUserById(this.userId).then (
        res => {
          console.log(res);
          this.user = res;

          this.userForm.patchValue({
            emailAddress: this.user.emailAddress,
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            tenantName: this.user.tenant.tenantName,
            securityGroupId: this.user.securityGroupId
          });
        },
        err => {
          this.showSnack(err);
        }
    );
  }

  onSubmit(form: any) {
    console.log('Not yet implemented');
  }

  save() {

    (<FormArray>this.userForm.get('securityGroupIds')).removeAt(0);
    (<FormArray>this.userForm.get('securityGroupIds')).push(new FormControl(this.userForm.get('securityGroupId').value));

    console.log(this.userForm.value);
    this.services.users.updateUser(this.userForm.value).then (
        res => {
          this.showSnack('User successfully saved.');
          this.router.navigate(['user-list']);
        },
        err => {
          this.showSnack(err);
        }
    );
  }

  getSecurityGroups(){
    this.services.securityGroups.getSecurityGroups()
      .then(
        res => {
          this.securityGroups = res;
        },
        err => {
          console.log();
        }
      );
  }

  onViewUsers() {
    this.router.navigate(['user-list']);
  }

  showSnack(message: string) {
    this.snackBar.open(message, "OK", { duration: 5000 });
  }
}
