import { NgModule, Injectable, EventEmitter } from "@angular/core";
import { MatSnackBar } from '@angular/material';
import * as jspdf from 'jspdf';  
import * as html2canvas from 'html2canvas';
import { AuthService} from './auth.service';

@NgModule()
export class BaseService{
      constructor(public snackBar: MatSnackBar = null, public auth:AuthService){}

      public logout:EventEmitter<boolean> = new EventEmitter();
      public HandleError(err){
            console.log("handle error");

            if(err.error.status == "UNAUTHORIZED"){
                  this.auth.Logout();
            }
            
            if(err.error.errors)
                  return err.error.errors;
            
            if(err.message){
                  let errors = new Array<string>();
                  if(err.message.indexOf("(unknown url)") > 0){
                        errors.push("Unable to connect to the API, please try again.");
                  }else
                        errors.push(err.message);
                  return errors;
            }
      }

      generatePDF(ElementID, HeadingText, DocumentOutputName)
      {  
            if(HeadingText == null || DocumentOutputName == null)
            {
                  this.snackBar.open("Please make sure you have specified the HeadingText & Document Output Name for your PDF Generating", "OK", {
                        duration: 5000,
                        verticalPosition: 'top'
                      });
                  return;
            }
            var data = document.getElementById(ElementID);
            html2canvas(data).then(canvas => {
                  // Few necessary setting options  
                  let padding = 10;
                  var imgWidth = 300 - (padding*2 + 2);
                  var imgHeight = 50;
                  
                  // A4 size page of PDF  
                  let pdf = new jspdf('l', 'mm', 'a4');
                  let img = canvas.toDataURL('image/png');  

                  // pdf.addFont('Roboto.ttf', 'Roboto', 'normal');
                  pdf.text(padding, padding + 10, HeadingText);
                  pdf.text(padding, padding + 21, '_________________________________________________________________________________________');
                  pdf.addImage(img, 'PNG', padding, padding + 35, imgWidth, imgHeight);
            
                  // Generated PDF 
                  pdf.save(DocumentOutputName + '.pdf');   
            });
      }
}