import { NgModule } from "@angular/core";
import { RestServiceModule } from "../modules/rest.service.module";
import { SecurityGroupModel } from "../models/security.group.model";
import { Pageable } from "../models/pageable.model";
import { BaseService } from "./base.service";
import { AuthService} from './auth.service';
import { MatSnackBar } from '@angular/material';

@NgModule()
export class SecurityGroupsService extends BaseService {
    constructor(private rest:RestServiceModule, public snackBar: MatSnackBar, public auth:AuthService){
        super(snackBar, auth);
    }

    public searchSecurityGroups(page: number, size: number) : Promise<Pageable<SecurityGroupModel>>{
        return new Promise<Pageable<SecurityGroupModel>>((resolve, reject) => {
            this.rest.securityGroups.searchSecurityGroups(page, size)
                .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public getSecurityGroupsByUser() : Promise<Array<SecurityGroupModel>>{
        return new Promise<Array<SecurityGroupModel>>((resolve, reject) => {
            this.rest.securityGroups.getSecurityGroupsByUser()
                .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public createSecurityGroups(securityGroup: SecurityGroupModel) : Promise<SecurityGroupModel>{
        return new Promise<SecurityGroupModel>((resolve, reject) => {
            this.rest.securityGroups.createSecurityGroup(securityGroup)
                .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public getSecurityGroup(securityGroupId: string) : Promise<SecurityGroupModel>{
        return new Promise<SecurityGroupModel>((resolve, reject) => {
            this.rest.securityGroups.getSecurityGroup(securityGroupId)
            .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public updateSecurityGroup(securityGroup: SecurityGroupModel) : Promise<SecurityGroupModel>{
        return new Promise<SecurityGroupModel>((resolve, reject) => {
            this.rest.securityGroups.updateSecurityGroup(securityGroup)
                .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public deleteSecurityGroup(securityGroupId: string) : Promise<SecurityGroupModel>{
        return new Promise<SecurityGroupModel>((resolve, reject) => {
            this.rest.securityGroups.deleteSecurityGroup(securityGroupId)
                .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public getSecurityGroups() : Promise<Array<SecurityGroupModel>>{
        return new Promise<Array<SecurityGroupModel>>((resolve, reject) => {
            this.rest.securityGroups.getSecurityGroups()
            .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }
}
