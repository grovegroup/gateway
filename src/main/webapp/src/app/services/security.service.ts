import { NgModule } from "@angular/core";
import { RestServiceModule } from "../modules/rest.service.module";
import { RegisterModel } from "../models/register.model";
import { AuthModel } from "../models/auth.model";
import { PasswordResetModel } from "../models/password.reset.model";
import { TokenModel } from "../models/token.model";
import { BaseService } from "./base.service";
import { AuthService} from './auth.service';
import { MatSnackBar } from '@angular/material';

@NgModule()
export class SecurityService extends BaseService {
    constructor(public rest:RestServiceModule, public snackBar: MatSnackBar, public auth:AuthService){
        super(snackBar, auth);
    }

    public register(registerModel: RegisterModel) : Promise<TokenModel>{
        return new Promise<TokenModel>((resolve, reject) => {
            this.rest.security.registerUser(registerModel)
                .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public async authenticate(authModel: AuthModel) : Promise<TokenModel> {
        return new Promise<TokenModel>((resolve, reject) => {
        this.rest.security.authenticate(authModel)
            .then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public requestPasswordReset(resetModel: PasswordResetModel) : Promise<PasswordResetModel> {
        return new Promise<PasswordResetModel>((resolve, reject) => {
             this.rest.security.requestPasswordReset(resetModel)
             .then((res) => {
                resolve(res.data);
             },
             (err) => {
                reject(this.HandleError(err));
             });
        });
    }

    public resetPassword(resetModel: PasswordResetModel) : Promise<PasswordResetModel>{
        return new Promise<PasswordResetModel>((resolve, reject) => {
            this.rest.security.resetPassword(resetModel)
            .then((res) => {
               resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
       });
    }
}