import { NgModule } from "@angular/core";
import { RestServiceModule } from "../modules/rest.service.module";
import { BaseService } from "./base.service";
import { Pageable } from "../models/pageable.model";
import { AuthService} from './auth.service';
import { MatSnackBar } from '@angular/material';
import { SearchRequestDTO } from "../models/search/search.request.dto";
import { TenantResponseDTO } from "../dto/response/tenant.response.dto";
import { TenantModel } from "../models/tenant.model";
import { TenantRequestDTO } from "../dto/request/tenant.request.dto";

@NgModule()
export class TenantService extends BaseService {

    constructor(public rest: RestServiceModule, public snackBar: MatSnackBar, public auth:AuthService) {
        super(snackBar,auth);
    }

    public async getTenantById(tenantId: string): Promise<TenantResponseDTO> {
        return new Promise<TenantResponseDTO>((resolve, reject) => {
            this.rest.tenant.getTenantById(tenantId)
                .then((res) => {
                    resolve(res.data);
                },
                (err) => {
                    reject(this.HandleError(err));
                });
        });
    }

    public createTenant(tenant: TenantRequestDTO): Promise<TenantRequestDTO> {
        return new Promise<TenantRequestDTO>((resolve, reject) => {
            this.rest.tenant.createTenant(tenant)
                .then((res) => {
                    resolve(res.data);
                },
                    (err) => {
                        reject(this.HandleError(err));
                    });
        });
    }

    public updateTenant(tenant: TenantModel): Promise<TenantModel> {
        return new Promise<TenantModel>((resolve, reject) => {
            this.rest.tenant.updateTenant(tenant)
                .then((res) => {
                    resolve(res.data);
                },
                    (err) => {
                        reject(this.HandleError(err));
                    });
        });
    }

    public updateStatus(tenantId: string, status: boolean): Promise<TenantModel> {
        return new Promise<TenantModel>((resolve, reject) => {
            this.rest.tenant.updateStatus(tenantId, status)
                .then((res) => {
                    resolve(res.data);
                },
                    (err) => {
                        reject(this.HandleError(err));
                    });
        });
    }

    public getAllTenants(): Promise<Array<TenantModel>> {
        return new Promise<Array<TenantModel>>((resolve, reject) => {
            this.rest.tenant.getAllTenants()
                .then((res) => {
                    resolve(res.data);
                },
                (err) => {
                    reject(this.HandleError(err));
                });
        });
    }

    public async searchTenants(searchCriteria: SearchRequestDTO): Promise<Pageable<TenantResponseDTO>> {
      return new Promise<Pageable<TenantResponseDTO>>((resolve, reject) => {
          this.rest.tenant.searchTenants(searchCriteria)
              .then((res) => {
                  resolve(res.data);
              },
              (err) => {
                  reject(this.HandleError(err));
              });
      });
  }
}
