import { NgModule } from "@angular/core";
import { RestServiceModule } from "../modules/rest.service.module";
import { BaseService } from "./base.service";
import { Pageable } from "../models/pageable.model";
import { AuthService} from './auth.service';
import { MatSnackBar } from '@angular/material';
import { LookupResponseDTO } from "../dto/response/lookup.response.dto";
import { LookupSearchRequestDTO } from "../models/search/lookup.search.request.dto";

@NgModule()
export class LookupService extends BaseService {

    constructor(public rest: RestServiceModule, public snackBar: MatSnackBar, public auth:AuthService) {
        super(snackBar,auth);
    }

    public async getLookupOptions(searchCriteria: LookupSearchRequestDTO): Promise<Pageable<LookupResponseDTO>> {
        return new Promise<Pageable<LookupResponseDTO>>((resolve, reject) => {
            this.rest.lookup.getLookupOptions(searchCriteria)
            .then((res) => {
                resolve(res.data);
              },
              (err) => {
                  reject(this.HandleError(err));
            });
        });
    }

    public getLookupOptionsByType(type: string): Promise<Array<LookupResponseDTO>> {
        return new Promise<Array<LookupResponseDTO>>((resolve, reject) => {
          this.rest.lookup.getLookupOptionsByType(type)
            .then((res) => {
                resolve(res.data);
              },
              (err) => {
                  reject(this.HandleError(err));
            });
        });
    }
}
