import { NgModule } from "@angular/core";
import { RestServiceModule } from "../modules/rest.service.module";
import { BaseService } from "./base.service";
import { UserModel } from "../models/user.model";
import { Pageable } from "../models/pageable.model";
import { AuthService} from './auth.service';
import { MatSnackBar } from '@angular/material';
import { UserProfileResponseDTO } from "../dto/response/user.profile.response.dto";
import { UserSearchRequestDTO } from "../models/search/user.search.request.dto";

@NgModule()
export class UserService extends BaseService {

    constructor(public rest: RestServiceModule, public snackBar: MatSnackBar, public auth:AuthService) {
        super(snackBar,auth);
    }

    public async getMe(): Promise<UserModel> {
        return new Promise<UserModel>((resolve, reject) => {
            this.rest.users.getMe()
                .then((res) => {
                    resolve(res.data);
                },
                (err) => {
                    reject(this.HandleError(err));
                });
        });
    }

    public async getUserById(userId: string): Promise<UserProfileResponseDTO> {
        return new Promise<UserProfileResponseDTO>((resolve, reject) => {
            this.rest.users.getUserById(userId)
                .then((res) => {
                    resolve(res.data);
                },
                    (err) => {
                        reject(this.HandleError(err));
                    });
        });
    }

    public createUser(user: UserModel): Promise<UserModel> {
        return new Promise<UserModel>((resolve, reject) => {
            this.rest.users.createUser(user)
                .then((res) => {
                    resolve(res.data);
                },
                    (err) => {
                        reject(this.HandleError(err));
                    });
        });
    }

    public updateUser(user: UserModel): Promise<UserModel> {
        return new Promise<UserModel>((resolve, reject) => {
            this.rest.users.updateUser(user)
                .then((res) => {
                    resolve(res.data);
                },
                    (err) => {
                        reject(this.HandleError(err));
                    });
        });
    }

    public updateStatus(userId: string, status: boolean): Promise<UserModel> {
        return new Promise<UserModel>((resolve, reject) => {
            this.rest.users.updateStatus(userId, status)
                .then((res) => {
                    resolve(res.data);
                },
                    (err) => {
                        reject(this.HandleError(err));
                    });
        });
    }

    public async searchUsers(searchCriteria: UserSearchRequestDTO): Promise<Pageable<UserProfileResponseDTO>> {
      return new Promise<Pageable<UserProfileResponseDTO>>((resolve, reject) => {
          this.rest.users.searchUsers(searchCriteria)
              .then((res) => {
                  resolve(res.data);
              },
              (err) => {
                  reject(this.HandleError(err));
              });
      });
    }
}
