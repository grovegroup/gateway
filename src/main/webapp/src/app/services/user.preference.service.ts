import { NgModule } from '@angular/core';

@NgModule()
export class UserPreferenceService {

  defaultItemsPerPage: string = '50';

  SetUserPreference(key: string, value: any){
      localStorage.setItem(key, value);
  }

  GetItemsPerPage(key: string) : any{
      let userPreference = localStorage.getItem(key);
      return userPreference === null ? this.defaultItemsPerPage : userPreference;
  }
}
