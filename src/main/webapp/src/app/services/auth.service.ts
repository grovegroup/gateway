import { Injectable, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from '../models/user.model';

@NgModule()
export class AuthService {

    public token: string = localStorage.getItem('authToken');
    public authed: string = localStorage.getItem('authed');
    public uid: string = localStorage.getItem('uid');
    public user: UserModel;
    public longitude: string;
    public latitude: string;

    constructor(private _router: Router){}

    isAuthenticated():boolean{

      if(this.token == null)
            this.GetToken();

        if(this.token != null && this.token != ''){
            this.GetProfileUid();
            return true;
        }

        return false;
    }

    SetAuthenticated(authed:boolean){
        if(authed)
            localStorage.setItem('authed', 'Y');
        else
            localStorage.setItem('authed', 'N');
    }

    GetAuthenticated(){
        var authed = localStorage.getItem('authed');
        if(authed == null || authed == '')
            this.authed = 'N';
        else if(authed == 'Y')
            this.authed = 'Y';
    }

    SetToken(token:string){
        localStorage.setItem('authToken', token);
        this.token = token;
    }

    GetToken(){
        this.token = localStorage.getItem('authToken');
        return this.token;
    }

    SetProfile(user:UserModel){
        localStorage.setItem('user', user.toJson());
    }

    GetProfile(){
        this.user = <UserModel>JSON.parse(localStorage.getItem('user'));
        return this.user;
    }

    SetProfileUid(uid:string){
        localStorage.setItem('uid', uid);
    }

    GetProfileUid(){
        this.uid = localStorage.getItem('uid');
        return this.uid;
    }

    Logout(){
        localStorage.clear();
        this.token = null;
        this.uid = null;
        this.authed = 'N';
        this.user = null;
        this._router.navigate(['login']);
    }

    SetCurrentLocation(){

        window.navigator.geolocation.getCurrentPosition(position => {
            localStorage.setItem('longitude', JSON.stringify(position.coords.longitude));
            localStorage.setItem('latitude', JSON.stringify(position.coords.latitude));
            console.log(JSON.parse(localStorage.getItem('position')));
        });
    }

    GetCurrentLatitude(){
        return JSON.parse(localStorage.getItem('latitude'));
    }

    GetCurrentLongitude(){
        return JSON.parse(localStorage.getItem('longitude'));
    }
}
