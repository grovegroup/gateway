import { NgModule } from "@angular/core";
import { RestServiceModule } from "../modules/rest.service.module";
import { BaseService } from "./base.service";
import { MenuModel } from "../models/menu.model";
import { resolve } from "url";
import { MenuItemModel } from "../models/menu.item.model";
import { AuthService} from './auth.service';
import { MatSnackBar } from '@angular/material';

@NgModule()
export class MenuService extends BaseService {
    constructor(public rest:RestServiceModule, public snackBar: MatSnackBar, public auth: AuthService){
        super(snackBar, auth);
    }

    public getMenus() : Promise<MenuModel[]>{
        return new Promise<MenuModel[]>((resolve, reject) => {
            this.rest.menu.getMenus().then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public getMenu(menuId: string) : Promise<MenuModel>{
        return new Promise<MenuModel>((resolve, reject) => {
            this.rest.menu.getMenu(menuId).then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public createMenu(menu:MenuModel) : Promise<MenuModel>{
        return new Promise<MenuModel>((resolve, reject) => {
            this.rest.menu.createMenu(menu).then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public updateMenu(menu:MenuModel) : Promise<MenuModel>{
        return new Promise<MenuModel>((resolve, reject) => {
            this.rest.menu.updateMenu(menu).then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public deleteMenu(menuId:string) : Promise<MenuModel>{
        return new Promise<MenuModel>((resolve, reject) => {
            this.rest.menu.deleteMenu(menuId).then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public createMenuItem(menuItem:MenuItemModel) : Promise<MenuItemModel>{
        return new Promise<MenuItemModel>((resolve, reject) => {
            this.rest.menu.createMenuItem(menuItem).then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public updateMenuItem(menuItem:MenuItemModel) : Promise<MenuItemModel>{
        return new Promise<MenuItemModel>((resolve, reject) => {
            this.rest.menu.updateMenuItem(menuItem).then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }

    public deleteMenuItem(menuItemId:string) : Promise<MenuItemModel>{
        return new Promise<MenuItemModel>((resolve, reject) => {
            this.rest.menu.deleteMenuItem(menuItemId).then((res) => {
                resolve(res.data);
            },
            (err) => {
                reject(this.HandleError(err));
            });
        });
    }
}