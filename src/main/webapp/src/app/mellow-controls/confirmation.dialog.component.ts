import { Component, OnInit, OnChanges, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'confirmation-dialog',
    template: `
        <div>
            <h2 mat-dialog-title>{{title}}</h2>
            <mat-dialog-content>
                {{data}}
            </mat-dialog-content>
            <br>
            <br>
            <mat-dialog-actions>
                <button mat-raised-button (click)="onConfirm()">OK</button>&nbsp;
                <button mat-raised-button (click)="onCancel()">Cancel</button>
            </mat-dialog-actions>
        </div>
    `,
    styleUrls: ["./styles/confirmation.dialog.component.css"]
})
export class ConfirmationDialogComponent {

    title;

    constructor(
        private dialogref : MatDialogRef<ConfirmationDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: string
    ) { }

    onConfirm() {
        this.dialogref.close(true);
    }

    onCancel() {
        this.dialogref.close(false);
    }

}