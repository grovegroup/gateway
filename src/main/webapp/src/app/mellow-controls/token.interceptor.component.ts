import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { AuthService } from "../services/auth.service";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import { tap } from "rxjs/operators";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(tap(event => {

    }, (error: HttpErrorResponse) => {
      if (error instanceof HttpErrorResponse) {
        if (error.status === 401) {
          this.authService.Logout();
          window.location.reload();
        }
      }
    }));

  }

  constructor(public authService: AuthService) {
  }

}