import { Component, OnInit } from "@angular/core";
import { UserModel } from "../models/user.model";
import { ServicesModule } from "../modules/services.module";

@Component({
      selector: 'toolbar',
      templateUrl: './views/toolbar.component.html',
      styleUrls: ['./styles/toolbar.component.css']
})
export class Toolbar implements OnInit{
      
      sidenav;
      user: UserModel;
      
      constructor(private services:ServicesModule){}

      ngOnInit(){
            this.user = this.services.auth.GetProfile();
      }
      
      logout(){
            this.services.auth.Logout();
      }
}