import { Component, Input, Output, OnChanges, ElementRef } from '@angular/core';
import { OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'mellow-drop-down',
    template: `
        <mat-form-field [appearance]="appearance">
            <mat-label>Select your {{placeholder}}</mat-label>
            <mat-select [placeholder]="placeholder" [disabled]="disabled" [required]="required">
                <mat-option *ngFor="let option of options" [value]="option" (click)="OptionSelected(option)">{{ option }}</mat-option>
            </mat-select>
        </mat-form-field>
    `,
    styleUrls: ["./styles/drop.down.component.css"]
})

export class DropDownComponent implements OnInit, OnChanges{

    @Input() placeholder: string;

    @Input() options: Array<string>;

    @Input() selection: string;

    @Input() index: string;
    
    // (legacy | standard) | fill | outline
    @Input() appearance: string;

    @Input() required: boolean = false;

    @Input() disabled: boolean = false;

    @Input() hint: string;

    @Output() optionChanged:EventEmitter<string> = new EventEmitter();

    constructor(){}

    ngOnInit(){
        console.log('Init');
        if(this.selection != null && this.selection != ''){
            this.selection = this.selection;
        }
    }

    ngOnChanges(changes) {
        if(changes.selection != null && changes.selection.currentValue != null){
            this.selection = changes.selection.currentValue;
        }

        if(changes.options != null && changes.options.currentValue != null)
            this.options = changes.options.currentValue;
    }

    OptionSelected(selection: string){
        this.selection = this.selection;
        this.optionChanged.emit(selection);
    }
}