import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';

@Component({
    selector: 'bottom-sheet',
    templateUrl: './bottom.sheet.html',
    styleUrls: ['./bottom.sheet.css'],
  })
  export class BottomSheetComponent {
    constructor(private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>) {}
    openLink(event: MouseEvent): void {
        this.bottomSheetRef.dismiss();
        event.preventDefault();
    }
  }