import { NgModule } from "@angular/core";
import { BaseRestService } from "./base.rest.service";
import { RestResponseModel } from "../models/rest.response.model";
import { Pageable } from "../models/pageable.model";
import { UserModel } from "../models/user.model";
import { map } from 'rxjs/operators';
import { MenuModel } from "../models/menu.model";
import { MenuItemModel } from "../models/menu.item.model";

@NgModule()
export class MenuRestService extends BaseRestService{
    
      public getMenus() : Promise<RestResponseModel<Array<MenuModel>>> {
            this.init();
            return this.get('menus/').pipe(map(data => {
                  let model = Object.assign(new Array<MenuModel>(), data.data);
                  return this.toResponse<Array<MenuModel>>(data, model);
            })).toPromise();
      }

      public getMenu(menuId:string) : Promise<RestResponseModel<MenuModel>> {
            this.init();
            return this.get('menus/' + menuId).pipe(map(data => {
                  let model = Object.assign(new MenuModel(), data.data);
                  return this.toResponse<MenuModel>(data, model);
            })).toPromise();
      }

      public createMenu(menu:MenuModel) : Promise<RestResponseModel<MenuModel>> {
            this.init();
            return this.post('menus/', menu.toJson()).pipe(map(data => {
                  let model = Object.assign(new MenuModel(), data.data);
                  return this.toResponse<MenuModel>(data, model);
            })).toPromise();
      }

      public updateMenu(menu:MenuModel) : Promise<RestResponseModel<MenuModel>> {
            this.init();
            return this.put('menus/', menu.toJson()).pipe(map(data => {
                  let model = Object.assign(new MenuModel(), data.data);
                  return this.toResponse<MenuModel>(data, model);
            })).toPromise();
      }

      public deleteMenu(menuId){
            this.init();
            return this.delete('menus/' + menuId).pipe(map(data => {
                  let model = Object.assign(new MenuModel(), data.data);
                  return this.toResponse<MenuModel>(data, model);
            })).toPromise();
      }

      public createMenuItem(menuItem:MenuItemModel){
            this.init();
            return this.post('menus/items', menuItem.toJson()).pipe(map(data => {
                  let model = Object.assign(new MenuItemModel(), data.data);
                  return this.toResponse<MenuItemModel>(data, model);
            })).toPromise();
      }

      public updateMenuItem(menuItem:MenuItemModel){
            this.init();
            return this.put('menus/items', menuItem.toJson()).pipe(map(data => {
                  let model = Object.assign(new MenuItemModel(), data.data);
                  return this.toResponse<MenuItemModel>(data, model);
            })).toPromise();
      }

      public deleteMenuItem(menuItemId){
            this.init();
            return this.delete('menus/items/' + menuItemId).pipe(map(data => {
                  let model = Object.assign(new MenuItemModel(), data.data);
                  return this.toResponse<MenuItemModel>(data, model);
            })).toPromise();
      }
}