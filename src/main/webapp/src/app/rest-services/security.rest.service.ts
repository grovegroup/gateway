import { Injectable, NgModule } from '@angular/core';
import { BaseRestService } from './base.rest.service';
import { AuthModel } from '../models/auth.model';
import { RegisterModel } from '../models/register.model';
import { PasswordResetModel } from '../models/password.reset.model';
import { RestResponseModel } from '../models/rest.response.model';
import { TokenModel } from '../models/token.model';
import { Observable } from 'rxjs';

@NgModule()
export class SecurityRestService extends BaseRestService {

    public registerUser(register: RegisterModel) : Promise<RestResponseModel<TokenModel>> {        
        return this.post("security/register", register.toJson()).toPromise();
    }

    public authenticate(authModel: AuthModel) : Promise<RestResponseModel<TokenModel>> {
        return this.post("security/authenticate", authModel.toJson()).toPromise();
    }

    public requestPasswordReset(resetModel: PasswordResetModel) : Promise<RestResponseModel<PasswordResetModel>> {
        return this.post("security/reset_password", resetModel.toJson()).toPromise();
    }

    public resetPassword(resetModel: PasswordResetModel) : Promise<RestResponseModel<PasswordResetModel>> {
        return this.put("security/reset_password", resetModel.toJson()).toPromise();
    }
}