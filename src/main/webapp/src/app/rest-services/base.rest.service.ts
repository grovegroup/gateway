import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { RestResponseModel } from '../models/rest.response.model';
import { map } from 'rxjs/operators';

@Injectable()
export class BaseRestService {

    public readonly BaseAPIurl: string = environment.BaseAPIurl;
    public headers:HttpHeaders;
    public options;

    defaultToken: string = "fleetControlGateway";

    constructor(
        public http: HttpClient,
        public authService: AuthService){}

    private log(message: string) {
        console.log(message);
    }

    /*
     *  Initialize the base request options
     */
    public init(){
        var userToken = this.authService.GetToken();

        if(userToken != null && userToken != "")
            this.defaultToken = userToken;

        this.headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.defaultToken
        });
        this.options = { headers: this.headers };
    }

    /*
     *  Base Service Call
     *  GET HTTP METHOD
     */
    public get(endpoint: string) : Observable<any> {
        this.init();
        return this.http.get(this.BaseAPIurl + endpoint, this.options);
    }

    /*
     *  Base Service Call
     *  POST HTTP METHOD
     */
    public post(endpoint: string, body: string) : Observable<any> {
        this.init();
        return this.http.post(this.BaseAPIurl + endpoint, body, this.options);
    }

    /*
     *  Base Service Call
     *  PUT HTTP METHOD
     */
    public put(endpoint: string, body: string) : Observable<any> {
        this.init();
        return this.http.put(this.BaseAPIurl + endpoint, body, this.options);
    }

    /*
     *  Base Service Call
     *  DELETE HTTP METHOD
     */
    public delete(endpoint: string) : Observable<any> {
        this.init();
        return this.http.delete(this.BaseAPIurl + endpoint, this.options);
    }

    public toResponse<T>(data:any, model:T) : RestResponseModel<T>{
        let r = new RestResponseModel<T>(
            data.status, data.message, data.errors, data.code, model, data.transactionId
        );
        return r;
    }
}
