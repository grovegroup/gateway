import { NgModule } from "@angular/core";
import { BaseRestService } from "./base.rest.service";
import { RestResponseModel } from "../models/rest.response.model";
import { Pageable } from "../models/pageable.model";
import { map } from 'rxjs/operators';
import { SearchRequestDTO } from "../models/search/search.request.dto";
import { TenantModel } from "../models/tenant.model";
import { TenantResponseDTO } from "../dto/response/tenant.response.dto";
import { TenantRequestDTO } from "../dto/request/tenant.request.dto";

@NgModule()
export class TenantRestService extends BaseRestService {

      public createTenant(tenant: TenantRequestDTO): Promise<RestResponseModel<TenantRequestDTO>> {
          this.init();
          return this.post('tenant/', JSON.stringify(tenant)).toPromise();
      }

      public updateTenant(tenant: TenantModel): Promise<RestResponseModel<TenantModel>> {
          this.init();
          return this.put('tenant/', JSON.stringify(tenant)).toPromise();
      }

      public updateStatus(tenantId: string, status: boolean): Promise<RestResponseModel<TenantModel>> {
          this.init();
          return this.put('tenant/status/' + tenantId + '/' + status, null).toPromise();
      }

      public getTenantById(tenantId: string): Promise<RestResponseModel<TenantResponseDTO>> {
          this.init();
          return this.get('tenant/' + tenantId).pipe(map(data => {
                let model = Object.assign(new TenantResponseDTO, data.data);
                return this.toResponse<TenantResponseDTO>(data, model);
            })).toPromise();
      }

      public getAllTenants(): Promise<RestResponseModel<Array<TenantResponseDTO>>> {
          this.init();
          return this.get('tenant/all').pipe(map(data => {
                let model = Object.assign(new Array<TenantResponseDTO>(), data.data);
                return this.toResponse<Array<TenantResponseDTO>>(data, model);
            })).toPromise();
      }

      public searchTenants(searchCriteria: SearchRequestDTO): Promise<RestResponseModel<Pageable<TenantResponseDTO>>> {
          return this.put('tenant/search' , searchCriteria.toJson()).pipe(map (data => {
                let i = 0;
                data.data.content.forEach(customer => {
                      let model = Object.assign(new TenantResponseDTO(), customer);
                      data.data[i] = model;
                      i++;
                });
                let model = Object.assign(new Pageable<TenantResponseDTO>(), data.data);
                return this.toResponse<Pageable<TenantResponseDTO>>(data, model);
          })).toPromise();
      }
}
