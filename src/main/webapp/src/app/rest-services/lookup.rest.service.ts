import { NgModule } from "@angular/core";
import { BaseRestService } from "./base.rest.service";
import { RestResponseModel } from "../models/rest.response.model";
import { Pageable } from "../models/pageable.model";
import { map } from 'rxjs/operators';
import { LookupResponseDTO } from "../dto/response/lookup.response.dto";
import { LookupSearchRequestDTO } from "../models/search/lookup.search.request.dto";

@NgModule()
export class LookupRestService extends BaseRestService {

      public getLookupOptions(searchCriteria: LookupSearchRequestDTO): Promise<RestResponseModel<Pageable<LookupResponseDTO>>> {
            return this.put('lookup/search', searchCriteria.toJson()).pipe(map(data => {
                  let i = 0;
                  data.data.content.forEach(order => {
                        let model = Object.assign(new LookupResponseDTO(), order);
                        data.data[i] = model;
                        i++;
                  });
                  let model = Object.assign(new Pageable<LookupResponseDTO>(), data.data);
                  return this.toResponse<Pageable<LookupResponseDTO>>(data, model);
            })).toPromise();
      }

      public getLookupOptionsByType(type: string): Promise<RestResponseModel<Array<LookupResponseDTO>>> {
            return this.get('lookup/' + type).pipe(map(data => {
                  let model = Object.assign(new Array<LookupResponseDTO>(), data.data);
                  return this.toResponse<Array<LookupResponseDTO>>(data, model);
            })).toPromise();
      }
}
