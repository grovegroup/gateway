import { NgModule } from "@angular/core";
import { BaseRestService } from "./base.rest.service";
import { RestResponseModel } from "../models/rest.response.model";
import { Pageable } from "../models/pageable.model";
import { SecurityGroupModel } from "../models/security.group.model";
import { map } from 'rxjs/operators';

@NgModule()
export class SecurityGroupsRestService extends BaseRestService{

    public searchSecurityGroups(page: number, size: number) : Promise<RestResponseModel<Pageable<SecurityGroupModel>>> {
        this.init();
        return this.post('security_groups/' + page + '/' + size, new SecurityGroupModel().toJson()).toPromise();
    }

    public getSecurityGroupsByUser() : Promise<RestResponseModel<Array<SecurityGroupModel>>> {
        this.init();
        return this.get('security_groups/get_by_user').pipe(map(data => {
            let model = Object.assign(new Array<SecurityGroupModel>(), data.data);
            return this.toResponse<Array<SecurityGroupModel>>(data, model);
        })).toPromise();
    }

    public createSecurityGroup(securityGroup: SecurityGroupModel) : Promise<RestResponseModel<SecurityGroupModel>> {
        this.init();
        return this.post('security_groups', securityGroup.toJson()).toPromise();
    }

    public getSecurityGroup(securityGroupId: string) : Promise<RestResponseModel<SecurityGroupModel>>{
        this.init();
        return this.get('security_groups/' +  securityGroupId).pipe(map(data => {
            let model = Object.assign(new SecurityGroupModel, data.data);
            return this.toResponse<SecurityGroupModel>(data, model);
        })).toPromise();
    }

    public updateSecurityGroup(securityGroup: SecurityGroupModel) : Promise<RestResponseModel<SecurityGroupModel>> {
        this.init();
        return this.put('security_groups', securityGroup.toJson()).toPromise();
    }

    public deleteSecurityGroup(securityGroupId: string) : Promise<RestResponseModel<SecurityGroupModel>>{
        this.init();
        return this.delete('security_groups/' +  securityGroupId).toPromise();
    }

    public getSecurityGroups() : Promise<RestResponseModel<Array<SecurityGroupModel>>>{
      this.init();
      return this.get('securityGroup').toPromise();
    }
}
