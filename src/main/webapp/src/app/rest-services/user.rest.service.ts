import { NgModule } from "@angular/core";
import { BaseRestService } from "./base.rest.service";
import { RestResponseModel } from "../models/rest.response.model";
import { Pageable } from "../models/pageable.model";
import { UserModel } from "../models/user.model";
import { map } from 'rxjs/operators';
import { UserProfileResponseDTO } from "../dto/response/user.profile.response.dto";
import { UserSearchRequestDTO } from "../models/search/user.search.request.dto";

@NgModule()
export class UserRestService extends BaseRestService {

      public getMe(): Promise<RestResponseModel<UserModel>> {
            this.init();
            return this.get('user/me').pipe(map(data => {
                  let model = Object.assign(new UserModel, data.data);
                  return this.toResponse<UserModel>(data, model);
              })).toPromise();
      }

      public getUserById(userId: string): Promise<RestResponseModel<UserProfileResponseDTO>> {
            this.init();
            return this.get('user/' + userId).pipe(map(data => {
                  let model = Object.assign(new UserProfileResponseDTO, data.data);
                  return this.toResponse<UserProfileResponseDTO>(data, model);
              })).toPromise();
      }

      public getUsers(page: number, size: number): Promise<RestResponseModel<Pageable<UserModel>>> {
            this.init();
            return this.post('users/' + page + '/' + size, new UserModel().toJson()).toPromise();
      }

      public createUser(user: UserModel): Promise<RestResponseModel<UserModel>> {
            this.init();
            return this.post('user/', JSON.stringify(user)).toPromise();
      }

      public updateUser(user: UserModel): Promise<RestResponseModel<UserModel>> {
            this.init();
            return this.put('user/', JSON.stringify(user)).toPromise();
      }

      public updateStatus(userId: string, status: boolean): Promise<RestResponseModel<UserModel>> {
            this.init();
            return this.put('user/status/' + userId + '/' + status, null).toPromise();
      }

      public searchUsers(searchCriteria: UserSearchRequestDTO): Promise<RestResponseModel<Pageable<UserProfileResponseDTO>>> {
        return this.put('user/search' , searchCriteria.toJson()).pipe(map (data => {
              let i = 0;
              data.data.content.forEach(customer => {
                    let model = Object.assign(new UserProfileResponseDTO(), customer);
                    data.data[i] = model;
                    i++;
              });
              let model = Object.assign(new Pageable<UserProfileResponseDTO>(), data.data);
              return this.toResponse<Pageable<UserProfileResponseDTO>>(data, model);
        })).toPromise();
  }
}
