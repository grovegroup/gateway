import { Injectable, NgModule } from '@angular/core';
import { BaseRestService } from './base.rest.service'
import { AuthModel } from '../models/auth.model';
import { AuthResponseModel } from '../models/auth.response.model';

@NgModule()
export class AuthRestService extends BaseRestService {

    public AuthenticateUser(model: AuthModel) {
        return this.post("security/authenticate", model.toJson());
    }
}
