import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxPaginationModule } from 'ngx-pagination'

import { AppComponent } from './components/app-component/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Browser } from 'protractor';
import { MaterialComponentsModule } from './modules/material.components.module';
import { AppRoutingModule } from './modules/app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { PasswordResetComponent } from './components/password-reset/password.reset.component';
import { HttpClientModule } from '@angular/common/http';
import { ServicesModule } from './modules/services.module';
import { RestServiceModule } from './modules/rest.service.module';
import { SecurityGroupsComponent } from './components/admin/security-groups/security.groups.component';
import { CreateSecurityGroupComponent } from './components/admin/security-groups/create.security.group.component';
import { EditSecurityGroupComponent } from './components/admin/security-groups/edit.security.group.component';
import { DropDownComponent } from './mellow-controls/drop.down.component';
import { Toolbar } from './mellow-controls/toolbar.component';
import { MenusComponent } from './components/admin/menus/menus.component';
import { CreateMenuComponent } from './components/admin/menus/create.menu.component';
import { EditMenuComponent } from './components/admin/menus/edit.menu.component';
import { CreateMenuItemComponent } from './components/admin/menus/create.menu.item.component';
import { BottomSheetComponent } from './mellow-controls/bottom-sheet/bottom.sheet.component';
import { UserListComponent } from './components/form-list-views/user-list/user.list.component';
import { UserCreateComponent } from './components/forms/user/user.create.component';
import { TenantListComponent } from './components/form-list-views/tenant-list/tenant.list.component';
import { TenantCreateComponent } from './components/forms/tenant/tenant.create.component';
import { UserUpdateComponent } from './components/form-views/user/user.update.component';
import { TenantUpdateComponent } from './components/form-views/tenant/tenant.update.component';
import { ConfirmDialogComponent } from './components/dialog/confirm-dialog/confirm.dialog.component';
import { ConfirmationDialogComponent } from './mellow-controls/confirmation.dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    PasswordResetComponent,
    SecurityGroupsComponent,
    CreateSecurityGroupComponent,
    EditSecurityGroupComponent,
    MenusComponent,
    CreateMenuComponent,
    EditMenuComponent,
    CreateMenuItemComponent,
    DropDownComponent,
    Toolbar,
    BottomSheetComponent,
    UserListComponent,
    UserCreateComponent,
    TenantListComponent,
    TenantCreateComponent,
    UserUpdateComponent,
    TenantUpdateComponent,
    ConfirmDialogComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ServicesModule,
    RestServiceModule,
    NgxPaginationModule
  ],
  exports: [],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent, ConfirmDialogComponent, BottomSheetComponent]
})
export class AppModule { }
