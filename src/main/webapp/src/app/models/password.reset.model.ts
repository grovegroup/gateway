import { BaseModel } from "./base.model";

export class PasswordResetModel extends BaseModel{

    constructor(
        public newPassword: string,
        public token: string,
        public emailAddress: string
    ){
        super();
    }
}