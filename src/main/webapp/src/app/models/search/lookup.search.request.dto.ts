import { SearchRequestDTO } from "./search.request.dto";

export class LookupSearchRequestDTO extends SearchRequestDTO {
    public lookupType: string;
    public data: string;

    constructor(lookupType: string, data: string, pageNumber: number, pageSize: number, transactionNumber: number, searchText: string) {
        super(searchText, pageNumber, pageSize, transactionNumber);
        this.lookupType = lookupType;
        this.data = data;
    }
}
