import { BaseModel } from "src/app/models/base.model";

export class SearchRequestDTO extends BaseModel {

    public pageNumber: number;
    public pageSize: number;
    public transactionNumber: number;
    public searchText: string;

    constructor(searchText: string, pageNumber: number, pageSize: number, transactionNumber: number) {
        super();
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.transactionNumber = transactionNumber;
        this.searchText = searchText;
    }
}
