import { SearchRequestDTO } from "./search.request.dto";

export class UserSearchRequestDTO extends SearchRequestDTO {
    public tenantId: string;

    constructor(tenantId: string, pageNumber: number, pageSize: number, transactionNumber: number, searchText: string) {
        super(searchText, pageNumber, pageSize, transactionNumber);
        this.tenantId = tenantId;
    }
}
