import { BaseModel } from "./base.model";

export class RegisterModel extends BaseModel {

    public emailAddress: string;
    public firstName: string;
    public lastName: string;
    public password: string;

    constructor() {
        super();
    }

    public static Create(
        emailAddress: string,
        firstName: string,
        lastName: string,
        password: string
    ) {
        let model = new RegisterModel();
        model.firstName = firstName;
        model.lastName = lastName;
        model.emailAddress = emailAddress;
        model.password = password;
        return model;
    }

}

