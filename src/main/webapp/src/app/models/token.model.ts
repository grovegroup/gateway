export class TokenModel{
    constructor(
      public authToken: string,
      public tokenType: string
    ){}
}
