import { BaseModel } from "./base.model";

export class Pageable<T> extends BaseModel{
    public content:T[];
    public totalPages:number;
    public first:boolean;
    public last:boolean;
    public totalElements: number;
    public size: number;
    public number: number;
    public pageable: any;
    public sort: any;
    public transactionNumber: number;
    constructor(){
        super();
    }
}
