import { BaseModel } from "./base.model";

export class VerificationModel extends BaseModel{

    constructor(
        public email: string,
        public otp: string
    ){
        super();
    }
}