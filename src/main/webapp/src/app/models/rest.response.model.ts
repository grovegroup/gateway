export class RestResponseModel<T>{

    constructor(
        public status: string,
        public message: string,
        public errors: string[],
        public code: number,
        public data: T,
        public transactionId: string
    ){}

    public hasErrors():boolean{
        return this.errors.length > 0;
    }
}