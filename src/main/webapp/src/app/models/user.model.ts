import { BaseModel } from './base.model';

export class UserModel extends BaseModel {
      public dateCreated: string;
      public dateUpdated: string;
      public userId: string;
      public tenantId: string;
      public tenantEnvironmentName: string;
      public emailAddress: string;
      public profileId: string;
      public securityGroupIds: string[];
      public firstName: string;
      public username: string;
      public lastName: string;

      constructor() {
            super();
      }

      public static Create(
            dateCreated: string,
            dateUpdated: string,
            userId: string,
            firstName: string,
            lastName: string,
            emailAddress: string,
            username: string,
            securityGroupIds: string[]) {
            let model = new UserModel();
            model.dateCreated = dateCreated;
            model.dateUpdated = dateUpdated;
            model.userId = userId;
            model.firstName = firstName;
            model.lastName = lastName;
            model.emailAddress = emailAddress;
            model.username = username;
            model.securityGroupIds = securityGroupIds;
            return model;
      }
}
