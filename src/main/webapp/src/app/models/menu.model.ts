import { MenuItemModel } from "./menu.item.model";
import { BaseModel } from "./base.model";

export class MenuModel extends BaseModel{
    public menuId: string;
    public menuItems: MenuItemModel[];
    public position: string;
    public title: string;

    constructor(){
        super();
    }

    public static Create(
        menuId: string,
        menuItems: MenuItemModel[],
        position: string,
        title: string
    ){
        let model = new MenuModel();
        model.menuId = menuId;
        model.menuItems = menuItems;
        model.position = position;
        model.title = title;
        return model;
    }
}