import { BaseModel } from "./base.model";

export class SecurityGroupModel extends BaseModel{
    public dateCreated: string;
    public dateUpdated: string;
    public deleted: boolean;
    public securityGroupId: string;
    public groupName: string;

    constructor(){
        super();
    }

    static Create(
        dateCreated: string, 
        dateUpdated:string, 
        deleted: boolean, 
        securityGroupId: string, 
        groupName: string){
            let model = new SecurityGroupModel();
            model.dateCreated = dateCreated;
            model.dateUpdated = dateUpdated;
            model.deleted = deleted;
            model.securityGroupId = securityGroupId;
            model.groupName = groupName;
            return model;
        }
}