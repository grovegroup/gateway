import { SecurityGroupModel } from "./security.group.model";
import { BaseModel } from "./base.model";

export class MenuItemModel extends BaseModel {
    public menuItemId: string;
    public icon: string;
    public menuId: string;
    public parentId: string;
    public path: string;
    public securityGroups: SecurityGroupModel[];
    public title: string;

    constructor(){
        super();
    }

    public static Create(
        menuItemId: string,
        icon: string,
        menuId: string, 
        parentId: string,
        path: string,
        securityGroups: SecurityGroupModel[],
        title: string
    ){
        let model = new MenuItemModel();
        model.menuItemId = menuItemId;
        model.icon = icon;
        model.menuId = menuId;
        model.parentId = parentId;
        model.path = path;
        model.securityGroups = securityGroups;
        model.title = title;
        return model;
    }
}