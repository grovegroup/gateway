import { BaseModel } from "./base.model";

export class AuthModel extends BaseModel{

    constructor(
        public emailAddress: string,
        public password: string
    ){
        super();
    }
}
