import { BaseModel } from "src/app/models/base.model";

export class TenantRequestDTO extends BaseModel {

    public tenantId: string;
    public tenantName: string;
    public databaseType: string;
}
