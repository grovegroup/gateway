import { BaseModel } from "src/app/models/base.model";

export class TenantResponseDTO extends BaseModel {

    public tenantId: string;
    public tenantEnvironmentName: string;
    public tenantName: string;
    public connectionString: string;
    public userName: string;
    public password: string;
    public driverClassName: string;
    public userCount: number;
    public deleted: boolean;
}
