import { BaseModel } from "src/app/models/base.model";
import { TenantResponseDTO } from "./tenant.response.dto";

export class UserProfileResponseDTO extends BaseModel {

    public userId: string;
    public profileId: string;
    public emailAddress: string;
    public firstName: string;
    public lastName: string;
    public tenant: TenantResponseDTO;
    public securityGroupId: string;
    public deleted: boolean;
}
