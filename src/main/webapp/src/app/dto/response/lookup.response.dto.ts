import { BaseModel } from "src/app/models/base.model";

export class LookupResponseDTO extends BaseModel {

    lookupType: string;
    shortCode: string;
    category: number;
    data: string;
}
