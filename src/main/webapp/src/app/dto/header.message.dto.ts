import { BaseModel } from "src/app/models/base.model";

export class HeaderMessageDTO extends BaseModel {

    public header: string;
    public message: string;
}
