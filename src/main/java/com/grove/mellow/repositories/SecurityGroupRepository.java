package com.grove.mellow.repositories;

import com.grove.mellow.models.SecurityGroup;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface SecurityGroupRepository  extends PagingAndSortingRepository<SecurityGroup,String> {

    Optional<SecurityGroup> findByGroupName(String groupName);

    List<SecurityGroup> findAllByGroupNameIsNotIn(List<String> groupNames);
}