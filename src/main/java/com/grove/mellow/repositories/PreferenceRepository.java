package com.grove.mellow.repositories;

import com.grove.mellow.models.Preference;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PreferenceRepository extends CrudRepository<Preference, String> {

    List<Preference> findAllByIsDeleted(boolean isDeleted);

    List<Preference> findAllByType(String type);
}
