package com.grove.mellow.repositories;

import com.grove.mellow.models.LookupOption;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface LookupOptionRepository extends PagingAndSortingRepository<LookupOption, String> {

    Optional<LookupOption> findByShortCode(String shortCode);

    Page<LookupOption> findAll(Specification<LookupOption> spec, Pageable pageable);

    List<LookupOption> findAllByLookupType(String lookupType);
}