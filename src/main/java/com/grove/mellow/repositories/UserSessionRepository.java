package com.grove.mellow.repositories;

import com.grove.mellow.models.UserSession;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSessionRepository extends CrudRepository<UserSession, String> {

    UserSession findByAuthToken(String authToken);

    UserSession findByUserId(String userId);

    @Modifying(flushAutomatically = true)
    @Query(value = "update user_session set is_valid = false where auth_token = :authToken", nativeQuery = true)
    void invalidateUserSession(@Param("authToken") String authToken);
}