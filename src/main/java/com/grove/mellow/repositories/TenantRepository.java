package com.grove.mellow.repositories;

import com.grove.mellow.models.Tenant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TenantRepository extends CrudRepository<Tenant, String> {

    List<Tenant> findAllByIsDeleted(boolean isDeleted);

    List<Tenant> findAllByIsDeletedAndTenantNameIsNotLike(boolean isDeleted, String notLike);

    Page<Tenant> findAll(Specification<Tenant> spec, Pageable pageable);

    Tenant findByTenantName(String tenantName);

    Optional<Tenant> findByTenantEnvironmentName(String tenantEnvironmentName);
}
