package com.grove.mellow.repositories;

import com.grove.mellow.models.SecurityGroup;
import com.grove.mellow.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Barend on 2018/07/19.
 * Handles all the repository sorting and paging
 */
@Repository
public interface UserRepository extends CrudRepository<User, String> {

    Optional<User> findByEmailAddress(String emailAddress);

    Optional<User> findByEmailAddressAndSecurityGroupsIn(String emailAddress, List<SecurityGroup> securityGroups);

    Page<User> findAll(Specification<User> spec, Pageable pageable);

    int countAllByTenantId(String tenantId);
}
