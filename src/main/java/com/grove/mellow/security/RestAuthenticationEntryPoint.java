package com.grove.mellow.security;

import com.google.gson.Gson;
import com.grove.mellow.dto.ResponseModelDTO;
import com.grove.mellow.services.BaseService;
import com.grove.mellow.utils.Constants;
import com.grove.mellow.utils.TransactionIdHelper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Barend on 2018/07/10.
 * This is fired if the JWT token is unauthorized
 */
@Component
public class RestAuthenticationEntryPoint extends BaseService implements AuthenticationEntryPoint {

    /**
     * Override Implement
     *
     * @param request
     * @param response
     * @param authException
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        logError(Constants.REST_UNAUTHORIZED, authException);
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        Gson gson = new Gson();
        ResponseModelDTO responseModelDTO = new ResponseModelDTO();
        responseModelDTO.setStatus(HttpStatus.UNAUTHORIZED);
        responseModelDTO.setMessage("Authentication failure!");
        responseModelDTO.setErrors(Arrays.asList(authException.getMessage()));
        responseModelDTO.setTransactionId(TransactionIdHelper.get());
        response.getOutputStream().println(gson.toJson(responseModelDTO));
    }
}