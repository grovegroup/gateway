package com.grove.mellow;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Initiates the Main Application
 */
public class ServletInitializer extends SpringBootServletInitializer {

    /**
     * Overrode Method to Start
     *
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MellowApplication.class);
    }

}
