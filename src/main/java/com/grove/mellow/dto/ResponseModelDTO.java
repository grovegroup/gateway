package com.grove.mellow.dto;


import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created by denzilbenjamin on 2018/07/17.
 */
public class ResponseModelDTO {

    /**
     * Fields
     */
    private HttpStatus status;
    private String message;
    private List<String> errors;
    private int code;
    private Object data;
    private String transactionId;

    /**
     * Constructors
     */
    public ResponseModelDTO() {
    }

    public ResponseModelDTO(HttpStatus status, String message, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.code = status.value();
    }

    public ResponseModelDTO(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList(error);
        this.code = status.value();
    }

    public ResponseModelDTO(HttpStatus status, String message, Object data) {
        super();
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList();
        this.data = data;
        this.code = status.value();
    }

    public ResponseModelDTO(HttpStatus status, String message, Object data, String error) {
        super();
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList(error);
        this.data = data;
        this.code = status.value();
    }

    public ResponseModelDTO(HttpStatus status, String message, Object data, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.data = data;
        this.code = status.value();
    }

    /**
     * Getters & Setters
     */
    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
