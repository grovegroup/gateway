package com.grove.mellow.dto.profile;

public class ProfileCreateDTO {

    private String profileId;
    private String userId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String tenantEnvironmentName;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getTenantEnvironmentName() {
        return tenantEnvironmentName;
    }

    public void setTenantEnvironmentName(String tenantEnvironmentName) {
        this.tenantEnvironmentName = tenantEnvironmentName;
    }
}
