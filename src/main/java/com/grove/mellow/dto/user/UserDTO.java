package com.grove.mellow.dto.user;

import com.grove.mellow.dto.search.PageContentDTO;
import com.grove.mellow.models.User;

public class UserDTO implements PageContentDTO{

    private String emailAddress;
    private String password;
    private String profileId;
    private String userId;
    private String tenantId;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public User toModel(){
        User user = new User();

        user.setEmailAddress(emailAddress);
        user.setPassword(password);
        user.setProfileId(profileId);
        user.setTenantId(tenantId);

        return user;
    }
}
