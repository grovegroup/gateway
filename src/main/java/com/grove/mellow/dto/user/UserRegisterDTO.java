package com.grove.mellow.dto.user;

import com.grove.mellow.dto.profile.ProfileCreateDTO;
import com.grove.mellow.models.User;
import com.grove.mellow.utils.GlobalMethods;

import java.util.List;

public class UserRegisterDTO {

    private String userId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private List<String> securityGroupIds;
    private String tenantId;
    private String tenantEnvironmentName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getSecurityGroupIds() {
        return securityGroupIds;
    }

    public void setSecurityGroupIds(List<String> securityGroupIds) {
        this.securityGroupIds = securityGroupIds;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantEnvironmentName() {
        return tenantEnvironmentName;
    }

    public void setTenantEnvironmentName(String tenantEnvironmentName) {
        this.tenantEnvironmentName = tenantEnvironmentName;
    }

    public User toModel(String tenantId){
        User user = new User();

        user.setEmailAddress(emailAddress);
        user.setTenantId(tenantId);

        return user;
    }

    public ProfileCreateDTO toProfileCreateDTO(){
        ProfileCreateDTO profileCreateDTO = new ProfileCreateDTO();

        profileCreateDTO.setEmailAddress(emailAddress);
        profileCreateDTO.setFirstName(firstName);
        profileCreateDTO.setLastName(lastName);
        profileCreateDTO.setProfileId(GlobalMethods.generateUuid());

        return profileCreateDTO;
    }
}
