package com.grove.mellow.dto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Barend on 2018/07/10.
 * The DTO for a rest response
 */
public class RestResponseDTO extends ResponseEntity<BaseDTO> {

    /**
     * Constants
     */
    public static final String RESPONSE_FAIL = "FAIL";
    public static final String RESPONSE_SUCCESS = "SUCCESS";
    public static final String RESPONSE_ERROR = "ERROR";

    /**
     * Fields
     */
    private String message;
    private List<String> errors;
    private Object data;
    private HttpStatus status;
    private String transactionId;
    private int code;

    /**
     * Constructor
     *
     * @param status
     */
    public RestResponseDTO(HttpStatus status) {
        super(status);
        this.status = status;
        this.code = status.value();
    }

    /**
     * Response Method
     *
     * @return
     */
    public ResponseEntity<?> Response() {
        ResponseModelDTO err = new ResponseModelDTO();
        err.setStatus(status);
        err.setCode(status.value());
        err.setData(data);
        err.setTransactionId(transactionId);
        err.setErrors(errors);
        err.setMessage(message);
        return status(status).body(err);
    }

    /**
     * Getters & Setters
     */
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
        this.code = status.value();
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
