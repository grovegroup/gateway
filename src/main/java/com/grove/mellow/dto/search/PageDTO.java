package com.grove.mellow.dto.search;

import org.springframework.data.domain.Sort;

import java.util.List;

public class PageDTO {

    private List<PageContentDTO> content;
    private List<?> extraContent;
    private Sort sort;
    private int pageNumber;
    private int entitiesOnPage;
    private long totalEntities;
    private int totalPages;
    private boolean isFirst;
    private boolean isLast;
    private Integer transactionNumber;

    public List<PageContentDTO> getContent() {
        return content;
    }

    public void setContent(List<PageContentDTO> content) {
        this.content = content;
    }

    public List<?> getExtraContent() {
        return extraContent;
    }

    public void setExtraContent(List<?> extraContent) {
        this.extraContent = extraContent;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getEntitiesOnPage() {
        return entitiesOnPage;
    }

    public void setEntitiesOnPage(int entitiesOnPage) {
        this.entitiesOnPage = entitiesOnPage;
    }

    public long getTotalEntities() {
        return totalEntities;
    }

    public void setTotalEntities(long totalEntities) {
        this.totalEntities = totalEntities;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }

    public Integer getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(Integer transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
}
