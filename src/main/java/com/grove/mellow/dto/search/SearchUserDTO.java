package com.grove.mellow.dto.search;

public class SearchUserDTO extends BaseSearchDTO {

    private String tenantId;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
}
