package com.grove.mellow.dto.search;

public class SearchLookupOptionsDTO extends BaseSearchDTO {

    private String lookupType;
    private String data;

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
