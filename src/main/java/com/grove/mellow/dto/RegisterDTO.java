package com.grove.mellow.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by Barend on 2018/07/10.
 * Data Transfer Object used to create an user
 */
public class RegisterDTO {

    /**
     * Users First Name
     */
    @NotBlank
    @Size(min = 2, max = 40)
    private String firstName;

    /**
     * Users Last Name
     */
    @NotBlank
    @Size(min = 2, max = 40)
    private String lastName;

    /**
     * Users Username
     */
    @Size(min = 3, max = 20)
    private String username;

    /**
     * Users Email Address
     */
    @NotBlank
    @Size(max = 60)
    @Email
    private String emailAddress;

    /**
     * MD5 Encrypted password
     */
    @NotBlank
    @Size(min = 6, max = 15)
    private String password;

    /**
     * The security group the user needs to belong to
     */
    private Set<String> securityGroupIds;

    /**
     * Getters & Setters
     */
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getSecurityGroupIds() {
        return securityGroupIds;
    }

    public void setSecurityGroupIds(Set<String> securityGroupIds) {
        this.securityGroupIds = securityGroupIds;
    }

}
