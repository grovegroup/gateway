package com.grove.mellow.dto;

/**
 * Created by Barend on 2018/07/10.
 * All other DTO's need to extend this BAseDTO
 */
public class BaseDTO {

    /**
     * Response Code used
     */
    private String responseCode;

    /**
     * Response Message used
     */
    private String responseMessage;

    /**
     * The transaction id to be used
     */
    private String transactionId;


    /**
     * Getters & Setters
     */
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

}
