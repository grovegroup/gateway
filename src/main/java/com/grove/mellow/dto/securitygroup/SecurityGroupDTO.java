package com.grove.mellow.dto.securitygroup;

import com.grove.mellow.models.SecurityGroup;
import com.grove.mellow.utils.GlobalMethods;

public class SecurityGroupDTO {

    private String securityGroupId;
    private String groupName;

    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public SecurityGroup toModel() {
        SecurityGroup model = new SecurityGroup();
        if(this.securityGroupId.equals(null)){
            model.setSecurityGroupId(GlobalMethods.generateUuid());
        } else {
            model.setSecurityGroupId(this.securityGroupId);
        }
        model.setGroupName(this.groupName);
        return model;
    }
}
