package com.grove.mellow.dto.login;

import java.util.List;

public class LoginResponseDTO {

    private String tenantEnvironmentName;
    private String profileId;
    private String authToken;
    private String[] securityRoles;

    public String getTenantEnvironmentName() {
        return tenantEnvironmentName;
    }

    public void setTenantEnvironmentName(String tenantEnvironmentName) {
        this.tenantEnvironmentName = tenantEnvironmentName;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String[] getSecurityRoles() {
        return securityRoles;
    }

    public void setSecurityRoles(String[] securityRoles) {
        this.securityRoles = securityRoles;
    }
}
