package com.grove.mellow.dto.login;

import javax.validation.constraints.NotBlank;

/**
 * Created by Barend on 2018/07/10.
 * Data Transfer Object used to login
 */
public class LoginRequestDTO {

    private String password;
    private String emailAddress;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
