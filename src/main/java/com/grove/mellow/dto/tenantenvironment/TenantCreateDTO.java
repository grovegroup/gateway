package com.grove.mellow.dto.tenantenvironment;

public class TenantCreateDTO {

    private String tenantName;
    private String databaseType;

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public void setDatabaseType(String databaseType) {
        this.databaseType = databaseType;
    }
}
