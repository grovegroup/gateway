package com.grove.mellow.dto.tenantenvironment;

import com.grove.mellow.dto.search.PageContentDTO;
import com.grove.mellow.models.Tenant;

public class TenantDTO implements PageContentDTO{

    private String tenantId;
    private String tenantEnvironmentName;
    private String tenantName;
    private String connectionString;
    private String userName;
    private String password;
    private String driverClassName;
    private int userCount;
    private boolean deleted;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantEnvironmentName() {
        return tenantEnvironmentName;
    }

    public void setTenantEnvironmentName(String tenantEnvironmentName) {
        this.tenantEnvironmentName = tenantEnvironmentName;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Tenant toModel(){
        Tenant tenant = new Tenant();

        tenant.setTenantEnvironmentName(tenantEnvironmentName);
        tenant.setTenantName(tenantName);
        tenant.setConnectionString(connectionString);
        tenant.setDriverClassName(driverClassName);
        tenant.setPassword(password);
        tenant.setUserName(userName);

        return tenant;
    }
}
