package com.grove.mellow.dto.lookup;

import com.grove.mellow.dto.search.PageContentDTO;

public class LookupDTO implements PageContentDTO {

    private String shortCode;
    private String category;
    private String data;

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
