package com.grove.mellow.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * Created by Barend on 2018/07/10.
 * Base model of all main models.
 * All other models should extend this model.
 */
@MappedSuperclass
public class BaseModel {

    /**
     * Date model was created
     */
    @Column(name = "dateCreated", updatable = false, insertable = false, columnDefinition = "DATETIME default CURRENT_TIMESTAMP NOT NULL ")
    @CreationTimestamp
    private Date dateCreated;

    /**
     * Date model was updated
     */
    @Column(name = "dateUpdated", insertable = false, columnDefinition = "DATETIME default CURRENT_TIMESTAMP NOT NULL")
    @UpdateTimestamp
    private Date dateUpdated;

    /**
     * Indicates whether entity has been deleted
     */
    @JsonIgnore
    @Column(name = "is_deleted", insertable = false, columnDefinition = "tinyint default 0 NOT NULL")
    private boolean isDeleted;

    /**
     * Getters & Setters
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
