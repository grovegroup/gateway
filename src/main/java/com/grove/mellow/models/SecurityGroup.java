package com.grove.mellow.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.grove.mellow.dto.securitygroup.SecurityGroupDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "security_group")
@XmlRootElement(name = "security_group")
public class SecurityGroup extends BaseModel {

    /**
     * Universal Unique Identifier
     * Ensures all models contains the UUID field
     */
    @Id
    @Column(name = "security_group_id", columnDefinition = "VARCHAR(36)")
    private String securityGroupId;

    /**
     * The name of the security group
     */
    @Column(name = "group_name", columnDefinition = "VARCHAR(35)")
    private String groupName;

    /**
     * Security Tag for Rest Security
     */
    @JsonIgnore
    @Column(name = "security_tag", columnDefinition = "VARCHAR(35) NOT NULL ")
    private String securityTag;

    /**
     * Getters & Setters
     */
    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSecurityTag() {
        return securityTag;
    }

    public void setSecurityTag(String securityTag) {
        this.securityTag = securityTag;
    }

    public SecurityGroupDTO toDTO() {
        SecurityGroupDTO dto = new SecurityGroupDTO();
        dto.setSecurityGroupId(this.securityGroupId);
        dto.setGroupName(this.groupName);
        return dto;
    }
}
