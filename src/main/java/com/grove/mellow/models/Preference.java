package com.grove.mellow.models;

import com.grove.mellow.dto.preference.PreferenceDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "preference")
@XmlRootElement(name = "preference")
public class Preference extends BaseModel {

    @Id
    @Column(name = "name", columnDefinition = "VARCHAR(100) NOT NULL ")
    private String name;

    @Column(name = "value", columnDefinition = "VARCHAR(10000) NOT NULL ")
    private String value;

    @Column(name = "type", columnDefinition = "VARCHAR(1) ")
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PreferenceDTO toDTO(){
        PreferenceDTO preferenceDTO = new PreferenceDTO();

        preferenceDTO.setName(name);
        preferenceDTO.setValue(value);

        return preferenceDTO;
    }
}

