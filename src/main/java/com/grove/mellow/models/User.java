package com.grove.mellow.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.grove.mellow.dto.user.UserDTO;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
@XmlRootElement(name = "user")
public class User extends BaseModel {

    @Id
    @Column(name = "user_id", columnDefinition = "VARCHAR(36)")
    private String userId;

    @Column(name = "email_address", columnDefinition = "VARCHAR(150)")
    private String emailAddress;

    @JsonIgnore
    @Column(name = "password", columnDefinition = "VARCHAR(60)")
    private String password;

    @Column(name = "profile_id", nullable = false, columnDefinition = "VARCHAR(36)")
    private String profileId;

    @Column(name = "tenant_id", nullable = false, columnDefinition = "VARCHAR(36)")
    private String tenantId;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<SecurityGroup> securityGroups = new HashSet<>();

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Set<SecurityGroup> getSecurityGroups() {
        return securityGroups;
    }

    public void setSecurityGroups(Set<SecurityGroup> securityGroups) {
        this.securityGroups = securityGroups;
    }

    public void addSecurityGroup(SecurityGroup securityGroup) {
        if (this.securityGroups != null) {
            this.securityGroups.add(securityGroup);
        } else {
            this.securityGroups = new HashSet<>();
            this.securityGroups.add(securityGroup);
        }
    }

    public UserDTO toDTO(){
        UserDTO userDTO = new UserDTO();

        userDTO.setEmailAddress(emailAddress);
        userDTO.setProfileId(profileId);
        userDTO.setTenantId(tenantId);
        userDTO.setUserId(userId);

        return userDTO;
    }
}
