package com.grove.mellow.models;

import com.grove.mellow.dto.lookup.LookupDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "lookup_option")
@XmlRootElement(name = "lookup_option")
public class LookupOption extends BaseModel {

    @Id
    @Column(name = "lookup_option_short_code", columnDefinition = "VARCHAR(10)")
    private String shortCode;

    @Column(name = "lookup_type", columnDefinition = "VARCHAR(200) NOT NULL ")
    private String lookupType;

    @Column(name = "category", columnDefinition = "VARCHAR(200)")
    private String category;

    @Column(name = "data", columnDefinition = "VARCHAR(200) NOT NULL ")
    private String data;

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public LookupDTO toDTO(){
        LookupDTO lookupDTO = new LookupDTO();

        lookupDTO.setCategory(category);
        lookupDTO.setData(data);
        lookupDTO.setShortCode(shortCode);

        return lookupDTO;
    }
}
