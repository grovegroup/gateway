package com.grove.mellow.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Entity
@Table(name = "user_session")
@XmlRootElement(name = "user_session")
public class UserSession  extends BaseModel {

    @Id
    @Column(name = "user_session_id", columnDefinition = "VARCHAR(36)")
    private String userSessionId;

    @Column(name = "user_id", nullable = false, columnDefinition = "VARCHAR(36)")
    private String userId;

    @Column(name = "auth_token", nullable = false, columnDefinition = "VARCHAR(36)")
    private String authToken;

    @Column(name = "email_address", columnDefinition = "VARCHAR(320)")
    private String emailAddress;

    @Column(name = "is_valid", columnDefinition = "tinyint default 1 NOT NULL")
    private boolean isValid;

    @Column(name = "expiry_date", columnDefinition = "DATETIME NOT NULL")
    private Date expiryDate;

    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
