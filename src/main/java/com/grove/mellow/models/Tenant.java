package com.grove.mellow.models;

import com.grove.mellow.dto.tenantenvironment.TenantDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "tenant")
@XmlRootElement(name = "tenant")
public class Tenant extends BaseModel {

    @Id
    @Column(name = "tenant_id", columnDefinition = "VARCHAR(36)")
    private String tenantId;

    @Column(name = "tenant_name", columnDefinition = "VARCHAR(150)")
    private String tenantName;

    @Column(name = "tenant_environment_name", columnDefinition = "VARCHAR(150)")
    private String tenantEnvironmentName;

    @Column(name = "connection_string", columnDefinition = "VARCHAR(300)")
    private String connectionString;

    @Column(name = "user_name", columnDefinition = "VARCHAR(100)")
    private String userName;

    @Column(name = "password", columnDefinition = "VARCHAR(100)")
    private String password;

    @Column(name = "driver_class_name", columnDefinition = "VARCHAR(100)")
    private String driverClassName;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantEnvironmentName() {
        return tenantEnvironmentName;
    }

    public void setTenantEnvironmentName(String tenantEnvironmentName) {
        this.tenantEnvironmentName = tenantEnvironmentName;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public TenantDTO toDTO(){
        TenantDTO tenantDTO = new TenantDTO();

        tenantDTO.setTenantEnvironmentName(tenantEnvironmentName);
        tenantDTO.setTenantId(tenantId);
        tenantDTO.setTenantName(tenantName);
        tenantDTO.setConnectionString(connectionString);
        tenantDTO.setDriverClassName(driverClassName);
        tenantDTO.setPassword(password);
        tenantDTO.setUserName(userName);
        tenantDTO.setDeleted(isDeleted());

        return tenantDTO;
    }
}
