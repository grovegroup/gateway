package com.grove.mellow.services;

import com.grove.mellow.dto.RegisterDTO;
import com.grove.mellow.dto.login.LoginRequestDTO;
import com.grove.mellow.dto.login.LoginResponseDTO;
import com.grove.mellow.dto.security.ResetPasswordDTO;
import com.grove.mellow.dto.user.UserDTO;
import com.grove.mellow.exceptions.RestException;

import javax.servlet.http.HttpSession;

/**
 * Created by Barend on 2018/07/10.
 * This is the interface for the user service
 */
public interface SecurityService {

    LoginResponseDTO login(LoginRequestDTO loginRequestDTO) throws RestException;

    LoginResponseDTO gatewayLogin(LoginRequestDTO loginRequestDTO) throws RestException;

    LoginResponseDTO register(RegisterDTO registerDTO) throws RestException;

    void logout(String authToken, HttpSession httpSession) throws RestException;

    UserDTO resetPassword(ResetPasswordDTO resetPasswordDTO) throws RestException;
}
