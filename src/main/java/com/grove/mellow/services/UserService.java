package com.grove.mellow.services;

import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.dto.search.SearchUserDTO;
import com.grove.mellow.dto.user.UserDTO;
import com.grove.mellow.dto.user.UserProfileDTO;
import com.grove.mellow.dto.user.UserRegisterDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.SecurityGroup;
import com.grove.mellow.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    void create(UserRegisterDTO userRegisterDTO) throws RestException;

    void update(UserRegisterDTO userRegisterDTO) throws RestException;

    void save(User user) throws RestException;

    void updateStatus(String userId, boolean status) throws RestException;

    UserProfileDTO getUserProfileDTO(String userId) throws RestException;

    UserDTO getUserProfileByEmailAddress(String emailAddress) throws RestException;

    UserDTO getMe() throws RestException;

    PageDTO search(SearchUserDTO searchDTO) throws RestException;

    User getById(String userId) throws RestException;

    Optional<User> findByEmailAddress(String emailAddress);

    Optional<User> findByEmailAddressAndSecurityGroupsIn(String emailAddress, List<SecurityGroup> secuityGroups);

    int getTenantUserCount(String tenantId) throws RestException;
}
