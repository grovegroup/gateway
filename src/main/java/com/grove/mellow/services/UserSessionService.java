package com.grove.mellow.services;

import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.UserSession;

public interface UserSessionService {

    UserSession getUserSessionByAuthToken(String authToken) throws RestException;

    UserSession getUserSessionByUserId(String userId) throws RestException;

    void save(UserSession userSession) throws RestException;

    void updateUserSessionTokenExpiryTime(String authToken) throws RestException;

    void logout(String authToken) throws RestException;
}
