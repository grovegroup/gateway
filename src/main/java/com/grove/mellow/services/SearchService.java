package com.grove.mellow.services;

import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.exceptions.RestException;
import org.springframework.data.domain.Page;

public interface SearchService {

    PageDTO getPage(Page<?> page, Integer transactionNumber) throws RestException;
}
