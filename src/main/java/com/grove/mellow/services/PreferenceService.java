package com.grove.mellow.services;

import com.grove.mellow.dto.preference.PreferenceDTO;
import com.grove.mellow.exceptions.RestException;

import java.util.List;

public interface PreferenceService {

    String getPreferenceStringValue(String preferenceName) throws RestException;

    List<PreferenceDTO> getPreferences(String type) throws RestException;

    long getPreferenceLongValue(String preferenceName) throws RestException;

    double getPreferenceDoubleValue(String preferenceName) throws RestException;

    boolean getPreferenceBooleanValue(String preferenceName) throws RestException;
}
