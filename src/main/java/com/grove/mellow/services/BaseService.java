package com.grove.mellow.services;

import com.google.cloud.logging.Severity;
import com.grove.mellow.models.User;
import com.grove.mellow.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Barend on 2018/07/09.
 * This is the base service.
 * Every other service implement should extend this base service.
 * In this service all logging functionality takes place.
 */
@Service
public class BaseService {

    protected HttpSession httpSession;

    @Autowired
    private void setHttpSession(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    /**
     * Indicates the environment defined in properties file
     */
    @Value("${environment}")
    protected String environment;

    /**
     * The main logging service of the app
     */
    private static LoggingService loggingService;

    /**
     * Have Spring autowire the logging service for us
     *
     * @param loggingService
     */
    @Autowired
    private void setLoggingService(LoggingService loggingService) {
        this.loggingService = loggingService;
    }

    /**
     * Local system logger
     */
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    /**
     * Logs an info item
     *
     * @param message
     */
    public void logInfo(String message) {
        LOGGER.log(Level.INFO, message);
        if (loggingService != null) {
            loggingService.writeLog(environment + " - " + message, Severity.INFO);
        }
    }

    /**
     * Logs a debugging item
     *
     * @param message
     */
    public void logDebug(String message) {
        LOGGER.log(Level.FINER, message);
        if (loggingService != null) {
            loggingService.writeLog(environment + " - " + message, Severity.DEBUG);
        }
    }

    /**
     * Logs an error with a message only
     *
     * @param message
     */
    public void logError(String message) {
        LOGGER.log(Level.SEVERE, message);
        if (loggingService != null) {
            loggingService.writeLog(environment + " - " + message, Severity.ERROR);
        }
    }

    /**
     * Logs an error with a message as well as a stacktrace for more information
     *
     * @param message
     * @param t
     */
    public void logError(String message, Throwable t) {
        LOGGER.log(Level.SEVERE, message, t);
        if (loggingService != null) {
            loggingService.writeLog(environment + " - " + message, Severity.ERROR);
        }
        if (loggingService != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            loggingService.writeLog(environment + " - " + message + " - " + sw.toString(), Severity.ERROR);
        }
    }

    /**
     * Logs an error with a stacktrace
     *
     * @param t
     */
    public void logError(Throwable t) {
        LOGGER.log(Level.SEVERE, t.getMessage(), t);
        if (loggingService != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            loggingService.writeLog(environment + " - " + sw.toString(), Severity.ERROR);
        }
    }

    protected User getCurrentUser() {
        return (User) httpSession.getAttribute(Constants.SECURITY_HTTP_SESSION_USER_ATTR);
    }
}
