package com.grove.mellow.services;

import com.grove.mellow.dto.securitygroup.SecurityGroupDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.SecurityGroup;

import java.util.List;
import java.util.Optional;

public interface SecurityGroupService {

    Optional<SecurityGroup> findById(String securityGroupId);

    Optional<SecurityGroup> findByGroupName(String groupName);

    List<SecurityGroupDTO> getSecurityGroups() throws RestException;
}
