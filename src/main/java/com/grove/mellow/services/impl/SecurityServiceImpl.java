package com.grove.mellow.services.impl;

import com.grove.mellow.dto.RegisterDTO;
import com.grove.mellow.dto.login.LoginRequestDTO;
import com.grove.mellow.dto.login.LoginResponseDTO;
import com.grove.mellow.dto.profile.ProfileCreateDTO;
import com.grove.mellow.dto.security.ResetPasswordDTO;
import com.grove.mellow.dto.user.UserDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.SecurityGroup;
import com.grove.mellow.models.Tenant;
import com.grove.mellow.models.User;
import com.grove.mellow.models.UserSession;
import com.grove.mellow.repositories.UserRepository;
import com.grove.mellow.services.*;
import com.grove.mellow.utils.Constants;
import com.grove.mellow.utils.GlobalMethods;
import com.grove.mellow.utils.Preferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Barend on 2018/07/10.
 * This is where are User logic happens
 */
@Service
public class SecurityServiceImpl extends BaseService implements SecurityService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    private TenantService tenantService;

    private UserService userService;

    private SecurityGroupService securityGroupService;

    private UserSessionService userSessionService;

    private PreferenceService preferenceService;

    @Autowired
    private void setPreferenceService(PreferenceService preferenceService) {
        this.preferenceService = preferenceService;
    }

    @Autowired
    private void setUserSessionService(UserSessionService userSessionService) {
        this.userSessionService = userSessionService;
    }

    @Autowired
    private void setSecurityGroupService(SecurityGroupService securityGroupService) {
        this.securityGroupService = securityGroupService;
    }

    @Autowired
    private void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    @Autowired
    private void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public LoginResponseDTO login(LoginRequestDTO loginRequestDTO) throws RestException {

        User user = userService.findByEmailAddress(loginRequestDTO.getEmailAddress()).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_CREDENTIALS_DOES_NOT_EXIST)
        );

        if(user.isDeleted()){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_CREDENTIALS_DOES_NOT_EXIST);
        }

        if(!passwordEncoder.matches(loginRequestDTO.getPassword(), user.getPassword())){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_INVALID_USERNAME_OR_PASSWORD);
        }

        LoginResponseDTO loginResponseDTO = new LoginResponseDTO();
        loginResponseDTO.setAuthToken(GlobalMethods.generateUuid());
        loginResponseDTO.setProfileId(user.getProfileId());

        Tenant tenant = tenantService.getById(user.getTenantId());

        if(tenant.isDeleted()){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.TENANT_DISABLED);
        }

        loginResponseDTO.setTenantEnvironmentName(tenant.getTenantEnvironmentName());

        if(user.getSecurityGroups().contains(securityGroupService.findByGroupName(Constants.ADMIN_SECURITY_GROUP_NAME).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_GROUP_DOES_NOT_EXIST)
        ))){
            createUserSession(user, loginResponseDTO.getAuthToken());
        }

        List<SecurityGroup> securityGroups = new ArrayList<>(user.getSecurityGroups());
        List<String> securityGroupNameList = securityGroups
                .stream()
                .map(sg -> sg.getGroupName())
                .collect(Collectors.toList());

        String[] securityGroupNames = securityGroupNameList.stream()
                             .toArray(String[]::new);
                             
        loginResponseDTO.setSecurityRoles(securityGroupNames);

        return loginResponseDTO;
    }

    @Override
    public LoginResponseDTO gatewayLogin(LoginRequestDTO loginRequestDTO) throws RestException {
        
        List<SecurityGroup> securityGroups = new ArrayList<>();

        securityGroups.add(securityGroupService.findByGroupName(Constants.OWNER_SECURITY_GROUP_NAME).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_GROUP_DOES_NOT_EXIST)
        ));

        User temp = userService.findByEmailAddress(loginRequestDTO.getEmailAddress()).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_CREDENTIALS_DOES_NOT_EXIST)
        );

        User user = userService.findByEmailAddressAndSecurityGroupsIn(loginRequestDTO.getEmailAddress(), securityGroups).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_CREDENTIALS_DOES_NOT_EXIST)
        );

        if(!passwordEncoder.matches(loginRequestDTO.getPassword(), user.getPassword())){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_INVALID_USERNAME_OR_PASSWORD);
        }

        LoginResponseDTO loginResponseDTO = new LoginResponseDTO();
        loginResponseDTO.setAuthToken(GlobalMethods.generateUuid());

        createUserSession(user, loginResponseDTO.getAuthToken());

        return loginResponseDTO;
    }

    private void createUserSession(User user, String authToken){
        httpSession.setAttribute(Constants.SECURITY_HTTP_SESSION_TOKEN_ATTR, authToken);
        httpSession.setAttribute(Constants.SECURITY_HTTP_SESSION_USER_ATTR, user);

        UserSession userSession = userSessionService.getUserSessionByUserId(user.getUserId());

        if(userSession == null){
            userSession = new UserSession();
            userSession.setUserSessionId(GlobalMethods.generateUuid());
        }

        userSession.setAuthToken(authToken);
        userSession.setEmailAddress(user.getEmailAddress());
        userSession.setUserId(user.getUserId());
        int tokenExpiryTime = (int)preferenceService.getPreferenceLongValue(Preferences.TokenExpiryHours);
        userSession.setExpiryDate(GlobalMethods.addTimeToCurrentDate(Calendar.HOUR, tokenExpiryTime));
        userSession.setValid(true);
        userSessionService.save(userSession);
    }

    @Override
    public LoginResponseDTO register(RegisterDTO registerDTO) throws RestException {

//        Optional<User> u = userService.findByEmailAddress(registerDTO.getEmailAddress());
//
//        if (u.isPresent())
//            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_ALREADY_EXISTS);
//
//        //Covert to model
//        User user = registerDTO.toModel();
//        user.setUserId(GlobalMethods.generateUuid());
//        user.setPassword(passwordEncoder.encode(userRegisterDTO.getPassword()));
//
//        Tenant tenant = tenantService.getById(userRegisterDTO.getTenantId());
//
//        ProfileCreateDTO profileCreateDTO = userRegisterDTO.toProfileCreateDTO();
//        profileCreateDTO.setUserId(user.getUserId());
//        profileCreateDTO.setTenantEnvironmentName(tenant.getTenantEnvironmentName());
//
//        createTenantProfile(profileCreateDTO);
//
//        user.setProfileId(profileCreateDTO.getProfileId());
//
//        //Set relational fields
//        if (userRegisterDTO.getSecurityGroupIds() != null && userRegisterDTO.getSecurityGroupIds().size() > 0) {
//            for (String securityGroupId : userRegisterDTO.getSecurityGroupIds()) {
//                user.addSecurityGroup(securityGroupService.findById(securityGroupId).orElseThrow(
//                        () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_GROUP_DOES_NOT_EXIST)
//                ));
//            }
//        } else {
//            //Defaults to normal user
//            user.addSecurityGroup(securityGroupService.findById(Constants.USER_SECURITY_GROUP_ID).orElseThrow(
//                    () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_GROUP_DOES_NOT_EXIST)
//            ));
//        }
//
//        userService.save(user);

        return null;
    }

    @Override
    @Transactional
    public void logout(String authToken, HttpSession httpSession) throws RestException {
        httpSession.invalidate();
        userSessionService.logout(authToken);
    }

    @Override
    public UserDTO resetPassword(ResetPasswordDTO resetPasswordDTO) throws RestException {
        User user = userService.getById(resetPasswordDTO.getUserId());

        user.setPassword(passwordEncoder.encode(resetPasswordDTO.getPassword()));

        userService.save(user);

        return user.toDTO();
    }
}
