package com.grove.mellow.services.impl;

import com.google.cloud.MonitoredResource;
import com.grove.mellow.services.LoggingService;
import org.springframework.stereotype.Service;
import com.google.cloud.logging.*;

import java.net.InetAddress;
import java.util.Collections;

/**
 * Created by Barend on 2018/07/09.
 */
@Service
public class LoggingServiceImpl implements LoggingService {

    /**
     * The logging instance for Google Cloud Logging
     */
    private Logging logging;

    /**
     * Gets the logging service
     *
     * @return
     * @throws Exception
     */
    private Logging getLoggingService() throws Exception {
        if (logging != null) {
            return logging;
        }
        LoggingOptions options = LoggingOptions.getDefaultInstance();
        logging = options.getService();
        return logging;
    }

    /**
     * Writes to logging service
     *
     * @param message
     * @param severity
     */
    @Override
    public void writeLog(String message, Severity severity) {
        String host = "host";
        try {
            //Obtain hostname of host
            host = InetAddress.getLocalHost().getHostName();
        } catch (Throwable t) {
        }
        LogEntry entry = LogEntry.newBuilder(Payload.StringPayload.of(message))
                .setLogName("app-" + host)
                .setSeverity(severity)
                .setResource(MonitoredResource.newBuilder("global").build())
                .build();
        try {
            getLoggingService().write(Collections.singleton(entry));
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
