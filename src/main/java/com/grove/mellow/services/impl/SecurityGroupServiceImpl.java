package com.grove.mellow.services.impl;

import com.grove.mellow.dto.securitygroup.SecurityGroupDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.SecurityGroup;
import com.grove.mellow.repositories.SecurityGroupRepository;
import com.grove.mellow.services.BaseService;
import com.grove.mellow.services.SecurityGroupService;
import com.grove.mellow.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SecurityGroupServiceImpl extends BaseService implements SecurityGroupService {

    private SecurityGroupRepository securityGroupRepository;

    @Autowired
    public void setSecurityGroupRepository(SecurityGroupRepository securityGroupRepository) {
        this.securityGroupRepository = securityGroupRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SecurityGroup> findById(String securityGroupId) {
        return securityGroupRepository.findById(securityGroupId);
    }

    @Override
    public Optional<SecurityGroup> findByGroupName(String groupName) {
        return securityGroupRepository.findByGroupName(groupName);
    }

    @Override
    public List<SecurityGroupDTO> getSecurityGroups() throws RestException {

        return securityGroupRepository.findAllByGroupNameIsNotIn(Constants.SECURITY_GROUPS_O)
            .stream()
            .map(SecurityGroup::toDTO)
            .collect(Collectors.toList());
    }
}
