package com.grove.mellow.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.grove.mellow.dto.ResponseModelDTO;
import com.grove.mellow.dto.profile.ProfileCreateDTO;
import com.grove.mellow.dto.search.ExternalPageDTO;
import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.dto.search.SearchUserDTO;
import com.grove.mellow.dto.user.UserDTO;
import com.grove.mellow.dto.user.UserProfileDTO;
import com.grove.mellow.dto.user.UserRegisterDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.SecurityGroup;
import com.grove.mellow.models.Tenant;
import com.grove.mellow.models.User;
import com.grove.mellow.repositories.UserRepository;
import com.grove.mellow.services.*;
import com.grove.mellow.utils.Constants;
import com.grove.mellow.utils.GlobalMethods;
import com.grove.mellow.utils.HttpClientHelper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class UserServiceImpl extends BaseService implements UserService {

    @Value("${fleetcontrolapplicationurl}")
    private String fleetControlApplicationUrl;

    @Value("${fleetcontrolapplicationdefaultdatabase}")
    private String fleetControlApplicationDefaultDatabase;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private SecurityGroupService securityGroupService;

    private UserRepository userRepository;

    private TenantService tenantService;

    private SearchService searchService;

    @Autowired
    private void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    @Autowired
    private void setSecurityGroupService(SecurityGroupService securityGroupService) {
        this.securityGroupService = securityGroupService;
    }

    @Autowired
    private void setTenantService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    @Autowired
    private void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void create(UserRegisterDTO userRegisterDTO) throws RestException {
        Optional<User> u = userRepository.findByEmailAddress(userRegisterDTO.getEmailAddress());

        if (u.isPresent())
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_ALREADY_EXISTS);

        Tenant tenant = null;
        if(!Strings.isNullOrEmpty(userRegisterDTO.getTenantId())){
            tenant = tenantService.getById(userRegisterDTO.getTenantId());
        }else if(!Strings.isNullOrEmpty(userRegisterDTO.getTenantEnvironmentName())){
            tenant = tenantService.getByTenantEnvironmentName(userRegisterDTO.getTenantEnvironmentName());
        }else {
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.TENANT__MISSING);
        }

        //Covert to model
        User user = userRegisterDTO.toModel(tenant.getTenantId());
        user.setUserId(GlobalMethods.generateUuid());
        user.setPassword(passwordEncoder.encode(userRegisterDTO.getPassword()));

        //Set relational fields
        if (!GlobalMethods.isListNullOrEmpty(userRegisterDTO.getSecurityGroupIds())) {
            for (String securityGroupId : userRegisterDTO.getSecurityGroupIds()) {
                user.addSecurityGroup(securityGroupService.findById(securityGroupId).orElseThrow(
                        () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_GROUP_DOES_NOT_EXIST)
                ));
            }
        } else{
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_ROLE_MISSING);
        }

        ProfileCreateDTO profileCreateDTO = userRegisterDTO.toProfileCreateDTO();
        profileCreateDTO.setUserId(user.getUserId());
        profileCreateDTO.setTenantEnvironmentName(tenant.getTenantEnvironmentName());

        saveTenantProfile(profileCreateDTO, 'C');

        user.setProfileId(profileCreateDTO.getProfileId());

        save(user);
    }

    @Override
    public void update(UserRegisterDTO userRegisterDTO) throws RestException {

        User user = getById(userRegisterDTO.getUserId());

        if(!userRegisterDTO.getEmailAddress().equalsIgnoreCase(user.getEmailAddress())){
            if (userRepository.findByEmailAddress(userRegisterDTO.getEmailAddress()).isPresent())
                throw RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_ALREADY_EXISTS);
        }

        user.getSecurityGroups().clear();

        if (!GlobalMethods.isListNullOrEmpty(userRegisterDTO.getSecurityGroupIds())) {
            for (String securityGroupId : userRegisterDTO.getSecurityGroupIds()) {
                user.addSecurityGroup(securityGroupService.findById(securityGroupId).orElseThrow(
                        () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_GROUP_DOES_NOT_EXIST)
                ));
            }
        } else{
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.SECURITY_ROLE_MISSING);
        }

        Tenant tenant = tenantService.getById(user.getTenantId());

        ProfileCreateDTO profileCreateDTO = userRegisterDTO.toProfileCreateDTO();
        profileCreateDTO.setUserId(user.getUserId());
        profileCreateDTO.setProfileId(user.getProfileId());
        profileCreateDTO.setTenantEnvironmentName(tenant.getTenantEnvironmentName());

        saveTenantProfile(profileCreateDTO, 'U');

        user.setEmailAddress(userRegisterDTO.getEmailAddress());

        save(user);

    }

    @Override
    public void save(User user) throws RestException {
        userRepository.save(user);
    }

    @Override
    public void updateStatus(String userId, boolean status) throws RestException {

        try{
            User user = getById(userId);

            user.setDeleted(status);

            Tenant tenant = tenantService.getById(user.getTenantId());

            Map<String,String> requestProperties = new HashMap<>();
            requestProperties.put("Authorization", Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION);
            requestProperties.put("Content-Type", Constants.JSON);
            requestProperties.put("Tenant-Environment", tenant.getTenantEnvironmentName());

            String result = HttpClientHelper.httpPut(fleetControlApplicationUrl + "profile/status/" + user.getProfileId() + "/" + status, requestProperties, "");

            ObjectMapper mapper = new ObjectMapper();

            ResponseModelDTO response = mapper.readValue(result, ResponseModelDTO.class);

            if(response.getCode() != 200){
                throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
            }

            save(user);
        }catch(Exception ex){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
        }

    }

    @Override
    public UserProfileDTO getUserProfileDTO(String userId) throws RestException {

        try{
            User user = getById(userId);

            Tenant tenant = tenantService.getById(user.getTenantId());

            Map<String,String> requestProperties = new HashMap<>();
            requestProperties.put("Authorization", Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION);
            requestProperties.put("Content-Type", Constants.JSON);
            requestProperties.put("Tenant-Environment", tenant.getTenantEnvironmentName());

            String result = HttpClientHelper.httpGet(fleetControlApplicationUrl + "profile/" + user.getProfileId(), requestProperties);

            ObjectMapper mapper = new ObjectMapper();

            ResponseModelDTO response = mapper.readValue(result, ResponseModelDTO.class);

            if(response.getCode() != 200){
                throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
            }

            UserProfileDTO userProfileDTO = mapper.convertValue(response.getData(), UserProfileDTO.class);
            userProfileDTO.setTenant(tenant.toDTO());
            userProfileDTO.setSecurityGroupId(user.getSecurityGroups().stream().findFirst().get().getSecurityGroupId());

            return userProfileDTO;
        }catch (Exception ex){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
        }
    }

    @Override
    public UserDTO getUserProfileByEmailAddress(String emailAddress) throws RestException {

        Optional<User> user = findByEmailAddress(emailAddress);

        if(!user.isPresent()){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
        }

        return user.get().toDTO();
    }

    @Override
    public UserDTO getMe() throws RestException {
        return getCurrentUser().toDTO();
    }

    @Override
    public PageDTO search(SearchUserDTO searchDTO) throws RestException {

        try{

            Tenant tenant = tenantService.getById(searchDTO.getTenantId());

            Map<String,String> requestProperties = new HashMap<>();
            requestProperties.put("Authorization", Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION);
            requestProperties.put("Content-Type", Constants.JSON);
            requestProperties.put("Tenant-Environment", tenant.getTenantEnvironmentName());

            String jsonString = new Gson().toJson(searchDTO);

            String result = HttpClientHelper.httpPut(fleetControlApplicationUrl + "profile/search", requestProperties, jsonString);

            ObjectMapper mapper = new ObjectMapper();

            ResponseModelDTO response = mapper.readValue(result, ResponseModelDTO.class);

            if(response.getCode() != 200){
                throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
            }

            String resultString = new Gson().toJson(response.getData());

            String contentString = new Gson().toJson(new JSONObject(resultString).toMap().get("content"));

            List<UserProfileDTO> userProfileDTOS = mapper.readValue(contentString, new TypeReference<List<UserProfileDTO>>() {});

            ExternalPageDTO externalPageDTO = mapper.readValue(resultString, ExternalPageDTO.class);

            PageDTO dto = externalPageDTO.toPageDTO();
            dto.setContent(new ArrayList<>());

            for(UserProfileDTO userProfileDTO : userProfileDTOS){
                userProfileDTO.setTenant(tenant.toDTO());
                dto.getContent().add(userProfileDTO);
            }

            return dto;
        }catch(Exception ex){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
        }

    }

    @Override
    public User getById(String userId) throws RestException {
        return userRepository.findById(userId).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.USER_DOES_NOT_EXIST)
        );
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findByEmailAddress(String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress);
    }

    @Override
    public Optional<User> findByEmailAddressAndSecurityGroupsIn(String emailAddress, List<SecurityGroup> securityGroups) {
        return userRepository.findByEmailAddressAndSecurityGroupsIn(emailAddress, securityGroups);
    }

    @Override
    public int getTenantUserCount(String tenantId) throws RestException {
        return userRepository.countAllByTenantId(tenantId);
    }

    private void createTenantProfile(ProfileCreateDTO profileCreateDTO){
        try{

            Map<String,String> requestProperties = new HashMap<>();
            requestProperties.put("Authorization", Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION);
            requestProperties.put("Content-Type", Constants.JSON);
            requestProperties.put("Tenant-Environment", fleetControlApplicationDefaultDatabase);

            String jsonString = new Gson().toJson(profileCreateDTO);

            String result = HttpClientHelper.httpPost(fleetControlApplicationUrl + "profile", requestProperties, jsonString);

            ObjectMapper mapper = new ObjectMapper();

            ResponseModelDTO response = mapper.readValue(result, ResponseModelDTO.class);

            if(response.getCode() != 200){
                throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
            }

        }catch(Exception ex){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
        }
    }

    private void saveTenantProfile(ProfileCreateDTO profileCreateDTO, Character action){
        try{

            Map<String,String> requestProperties = new HashMap<>();
            requestProperties.put("Authorization", Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION);
            requestProperties.put("Content-Type", Constants.JSON);
            requestProperties.put("Tenant-Environment", fleetControlApplicationDefaultDatabase);

            String jsonString = new Gson().toJson(profileCreateDTO);

            String result = null;

            switch(action){
                case 'C':
                    result = HttpClientHelper.httpPost(fleetControlApplicationUrl + "profile", requestProperties, jsonString);
                    break;
                case 'U':
                    result = HttpClientHelper.httpPut(fleetControlApplicationUrl + "profile", requestProperties, jsonString);
                    break;
            }

            ObjectMapper mapper = new ObjectMapper();

            ResponseModelDTO response = mapper.readValue(result, ResponseModelDTO.class);

            if(response.getCode() != 200){
                throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
            }

        }catch(Exception ex){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
        }
    }
}
