package com.grove.mellow.services.impl;

import com.grove.mellow.dto.preference.PreferenceDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.Preference;
import com.grove.mellow.repositories.PreferenceRepository;
import com.grove.mellow.services.BaseService;
import com.grove.mellow.services.PreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PreferenceServiceImpl extends BaseService implements PreferenceService {

    private PreferenceRepository preferenceRepository;

    @Autowired
    private void setPreferenceRepository(PreferenceRepository preferenceRepository) {
        this.preferenceRepository = preferenceRepository;
    }

    private static Map<String, String> currentPreferencesMap;

    private static Long lastPreferenceList = new Long(0);

    private void loadPreferences() {

        if (currentPreferencesMap != null && lastPreferenceList != 0) {
            long diff = System.currentTimeMillis() - lastPreferenceList;

            if (diff < (60000 * 5)) {
                return;
            }
        }

        lastPreferenceList = System.currentTimeMillis();

        HashMap currentPropertiesMap = new HashMap<>();
        for (PreferenceDTO dto : getEnvironmentProperties()) {
            currentPropertiesMap.put(dto.getName(), dto.getValue());

        }

        currentPreferencesMap = currentPropertiesMap;
    }

    private List<PreferenceDTO> getEnvironmentProperties() {

        List<PreferenceDTO> preferenceDTOList = new ArrayList();

        for (Preference preference : preferenceRepository.findAllByIsDeleted(false)) {
            preferenceDTOList.add(preference.toDTO());
        }

        return preferenceDTOList;
    }

    @Override
    public String getPreferenceStringValue(String preferenceName) throws RestException {
        loadPreferences();
        return currentPreferencesMap.get(preferenceName);
    }

    @Override
    public List<PreferenceDTO> getPreferences(String type) throws RestException {

        List<PreferenceDTO> preferenceDTOList = new ArrayList();

        for (Preference preference : preferenceRepository.findAllByType(type)) {
            preferenceDTOList.add(preference.toDTO());
        }

        return preferenceDTOList;
    }

    @Override
    public long getPreferenceLongValue(String preferenceName) throws RestException {
        loadPreferences();
        return new Long(currentPreferencesMap.get(preferenceName));
    }

    @Override
    public double getPreferenceDoubleValue(String preferenceName) throws RestException {
        loadPreferences();
        return new Double(currentPreferencesMap.get(preferenceName));
    }

    @Override
    public boolean getPreferenceBooleanValue(String preferenceName) throws RestException {
        loadPreferences();
        return new Boolean(currentPreferencesMap.get(preferenceName));
    }
}

