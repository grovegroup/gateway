package com.grove.mellow.services.impl;

import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.UserSession;
import com.grove.mellow.repositories.UserSessionRepository;
import com.grove.mellow.services.BaseService;
import com.grove.mellow.services.PreferenceService;
import com.grove.mellow.services.UserSessionService;
import com.grove.mellow.utils.GlobalMethods;
import com.grove.mellow.utils.Preferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

@Service
public class UserSessionServiceImpl extends BaseService implements UserSessionService {

    private UserSessionRepository userSessionRepository;

    private PreferenceService preferenceService;

    @Autowired
    private void setPreferenceService(PreferenceService preferenceService) {
        this.preferenceService = preferenceService;
    }

    @Autowired
    private void setUserSessionRepository(UserSessionRepository userSessionRepository) {
        this.userSessionRepository = userSessionRepository;
    }

    @Override
    public UserSession getUserSessionByAuthToken(String authToken) throws RestException {
        return userSessionRepository.findByAuthToken(authToken);
    }

    @Override
    public UserSession getUserSessionByUserId(String userId) throws RestException {
        return userSessionRepository.findByUserId(userId);
    }

    @Override
    public void save(UserSession userSession) throws RestException {
        userSessionRepository.save(userSession);
    }

    @Override
    public void updateUserSessionTokenExpiryTime(String authToken) throws RestException {
        try{
            int tokenExpiryTime = (int)preferenceService.getPreferenceLongValue(Preferences.TokenExpiryHours);
            UserSession userSession = getUserSessionByAuthToken(authToken);
            userSession.setExpiryDate(GlobalMethods.addTimeToCurrentDate(Calendar.HOUR, tokenExpiryTime));
            save(userSession);
        }catch(Exception ex){
            logError(ex);
        }
    }

    @Override
    @Transactional
    public void logout(String authToken) throws RestException {
        userSessionRepository.invalidateUserSession(authToken);
    }
}