package com.grove.mellow.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.grove.mellow.dto.ResponseModelDTO;
import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.dto.tenantenvironment.TenantCreateDTO;
import com.grove.mellow.dto.tenantenvironment.TenantDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.Tenant;
import com.grove.mellow.repositories.TenantRepository;
import com.grove.mellow.services.*;
import com.grove.mellow.specifications.TenantSpecification;
import com.grove.mellow.utils.Constants;
import com.grove.mellow.utils.GlobalMethods;
import com.grove.mellow.utils.HttpClientHelper;
import com.grove.mellow.utils.Preferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TenantServiceImpl extends BaseService implements TenantService {

    @Value("${fleetcontrolapplicationurl}")
    private String fleetControlApplicationUrl;

    @Value("${fleetcontrolapplicationdefaultdatabase}")
    private String fleetControlApplicationDefaultDatabase;

    private TenantRepository tenantRepository;

    private SearchService searchService;

    private UserService userService;

    private PreferenceService preferenceService;

    @Autowired
    private void setPreferenceService(PreferenceService preferenceService) {
        this.preferenceService = preferenceService;
    }

    @Autowired
    private void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    @Autowired
    private void setTenantRepository(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Autowired
    private void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void create(TenantCreateDTO tenantCreateDTO) throws RestException {

        try{
            Map<String,String> requestProperties = new HashMap<>();
            requestProperties.put("Authorization", Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION);
            requestProperties.put("Content-Type", Constants.JSON);
            requestProperties.put("Tenant-Environment", fleetControlApplicationDefaultDatabase);

            TenantDTO tenantDTO = createTenant(tenantCreateDTO);

            String jsonString = new Gson().toJson(tenantDTO);

            String result = HttpClientHelper.httpPost(fleetControlApplicationUrl + "tenant", requestProperties, jsonString);

            ObjectMapper mapper = new ObjectMapper();

            ResponseModelDTO response = mapper.readValue(result, ResponseModelDTO.class);

            if(response.getCode() != 200){
                throw RestException.getRestException(RestException.TYPE_ERROR, Constants.MESSAGE_GENERIC_ERROR);
            }

            Tenant tenant = tenantDTO.toModel();

            tenant.setTenantId(GlobalMethods.generateUuid());

            save(tenant);

            updateApplicationTenants();

        }catch(Exception ex){
            logError(ex);
            throw RestException.getRestException(RestException.TYPE_ERROR, ex.getMessage());
        }
    }

    @Override
    public void update(TenantDTO tenantDTO) throws RestException {
        Tenant tenant = getById(tenantDTO.getTenantId());

        if(tenantDTO.getTenantName().equalsIgnoreCase(tenant.getTenantName())){
            return;
        }else if(tenantRepository.findByTenantName(tenantDTO.getTenantName()) != null){
            throw RestException.getRestException(RestException.TYPE_ERROR, Constants.TENANT_NAME_EXISTS);
        }

        tenant.setTenantName(tenantDTO.getTenantName());

        save(tenant);
    }

    @Override
    public void save(Tenant tenant) throws RestException {
        tenantRepository.save(tenant);
    }

    @Override
    public void updateStatus(String tenantId, boolean isDeleted) throws RestException {
        Tenant tenant = getById(tenantId);
        tenant.setDeleted(isDeleted);

        save(tenant);

        updateApplicationTenants();
    }

    @Override
    public PageDTO search(BaseSearchDTO searchDTO) throws RestException {

        //User user = getCurrentUser();

        PageDTO dto = new PageDTO();

//        if(user.getSecurityGroups().
//                stream().
//                filter(sg -> sg.getGroupName().equalsIgnoreCase(Constants.OWNER_SECURITY_GROUP_NAME))
//                .findFirst().isPresent()){
//
//            TenantSpecification specs = new TenantSpecification(searchDTO);
//
//            Specification<Tenant> tenantSpec = specs.getSearchCriteria();
//
//            Page<Tenant> tenantPage = tenantRepository.findAll(tenantSpec,
//                    PageRequest.of(searchDTO.getPageNumber(), searchDTO.getPageSize(),
//                            Sort.by(Sort.Direction.ASC, "tenantName")));
//
//            dto = searchService.getPage(tenantPage, searchDTO.getTransactionNumber());
//
//            for (Tenant tenant : tenantPage.getContent()) {
//                TenantDTO tenantDTO = tenant.toDTO();
//                tenantDTO.setUserCount(userService.getTenantUserCount(tenant.getTenantId()));
//                dto.getContent().add(tenantDTO);
//            }
//
//        }else{
//            Tenant tenant = getById(user.getTenantId());
//            dto.setContent(new ArrayList<>());
//            TenantDTO tenantDTO = tenant.toDTO();
//            tenantDTO.setUserCount(userService.getTenantUserCount(tenant.getTenantId()));
//            dto.getContent().add(tenantDTO);
//        }

        TenantSpecification specs = new TenantSpecification(searchDTO);

        Specification<Tenant> tenantSpec = specs.getSearchCriteria();

        Page<Tenant> tenantPage = tenantRepository.findAll(tenantSpec,
                PageRequest.of(searchDTO.getPageNumber(), searchDTO.getPageSize(),
                        Sort.by(Sort.Direction.ASC, "tenantName")));

        dto = searchService.getPage(tenantPage, searchDTO.getTransactionNumber());

        for (Tenant tenant : tenantPage.getContent()) {
            TenantDTO tenantDTO = tenant.toDTO();
            tenantDTO.setUserCount(userService.getTenantUserCount(tenant.getTenantId()));
            dto.getContent().add(tenantDTO);
        }

        return dto;
    }

    @Override
    public Tenant getById(String tenantId) throws RestException {
        return tenantRepository.findById(tenantId).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.TENANT_DOES_NOT_EXIST)
        );
    }

    @Override
    public Tenant getByTenantEnvironmentName(String tenantEnvironmentName) throws RestException {
        return tenantRepository.findByTenantEnvironmentName(tenantEnvironmentName).orElseThrow(
                () -> RestException.getRestException(RestException.TYPE_ERROR, Constants.TENANT_DOES_NOT_EXIST)
        );
    }

    @Override
    public TenantDTO getTenantDTO(String tenantId) throws RestException {
        return getById(tenantId).toDTO();
    }

    @Override
    public List<TenantDTO> getAllTenants() throws RestException {

        List<Tenant> tenants = tenantRepository.findAllByIsDeletedAndTenantNameIsNotLike(false, "Default");

        List<TenantDTO> tenantDTOs = new ArrayList<>();

        for(Tenant tenant : tenants){
            tenantDTOs.add(tenant.toDTO());
        }

        return tenantDTOs;
    }

    @Override
    public List<TenantDTO> getAllTenantsForApplicationServer() throws RestException {
        List<Tenant> tenants = tenantRepository.findAllByIsDeleted(false);

        List<TenantDTO> tenantDTOs = new ArrayList<>();

        for(Tenant tenant : tenants){
            tenantDTOs.add(tenant.toDTO());
        }

        return tenantDTOs;
    }

    private void updateApplicationTenants(){
        try{
            Map<String,String> requestProperties = new HashMap<>();
            requestProperties.put("Authorization", Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION);
            requestProperties.put("Content-Type", Constants.JSON);
            requestProperties.put("Tenant-Environment", fleetControlApplicationDefaultDatabase);

            String result = HttpClientHelper.httpGet(fleetControlApplicationUrl + "tenant", requestProperties);

            ObjectMapper mapper = new ObjectMapper();

            ResponseModelDTO response = mapper.readValue(result, ResponseModelDTO.class);

        }catch(Exception ex){
            logError(ex);
            throw RestException.getRestException(RestException.TYPE_ERROR, ex.getMessage());
        }
    }

    private TenantDTO createTenant(TenantCreateDTO tenantCreateDTO){
        TenantDTO tenantDTO = new TenantDTO();
        tenantDTO.setTenantName(tenantCreateDTO.getTenantName());

        String environmentName = tenantCreateDTO.getTenantName().toLowerCase().replaceAll(" ", "_") + "_tower";
        tenantDTO.setTenantEnvironmentName(environmentName);

        switch(tenantCreateDTO.getDatabaseType()){
            case Constants.DATABASE_TYPE_MYSQL:
                tenantDTO.setConnectionString(preferenceService.getPreferenceStringValue(
                        Preferences.MySQLConnectionString).replace(Constants.DATABASE_NAME_PLACEHOLDER, environmentName));
                tenantDTO.setDriverClassName(preferenceService.getPreferenceStringValue(Preferences.MySQLDriverClassName));
                tenantDTO.setUserName(preferenceService.getPreferenceStringValue(Preferences.MySQLUserName));
                tenantDTO.setPassword(preferenceService.getPreferenceStringValue(Preferences.MySQLPassword));
                break;
            case Constants.DATABASE_TYPE_SQL:
                break;
            case Constants.DATABASE_TYPE_PGS:
                break;
            case Constants.DATABASE_TYPE_MDB:
                break;
        }

        return tenantDTO;
    }
}
