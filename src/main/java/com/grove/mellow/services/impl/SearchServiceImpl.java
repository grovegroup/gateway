package com.grove.mellow.services.impl;

import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.services.BaseService;
import com.grove.mellow.services.SearchService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class SearchServiceImpl extends BaseService implements SearchService {

    @Override
    public PageDTO getPage(Page<?> page, Integer transactionNumber) throws RestException {

        PageDTO dto = new PageDTO();
        dto.setPageNumber(page.getNumber());
        dto.setEntitiesOnPage(page.getNumberOfElements());
        dto.setTotalEntities(page.getTotalElements());
        dto.setTotalPages(page.getTotalPages());
        dto.setSort(page.getSort());
        dto.setFirst(page.isFirst());
        dto.setLast(page.isLast());
        dto.setContent(new ArrayList<>());
        if (transactionNumber != null) {
            dto.setTransactionNumber(transactionNumber);
        }

        return dto;
    }
}
