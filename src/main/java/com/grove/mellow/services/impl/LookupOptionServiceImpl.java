package com.grove.mellow.services.impl;

import com.grove.mellow.dto.lookup.LookupDTO;
import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.dto.search.SearchLookupOptionsDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.LookupOption;
import com.grove.mellow.repositories.LookupOptionRepository;
import com.grove.mellow.services.BaseService;
import com.grove.mellow.services.LookupOptionService;
import com.grove.mellow.services.SearchService;
import com.grove.mellow.specifications.LookupOptionSpecification;
import com.grove.mellow.utils.Constants;
import com.grove.mellow.utils.GlobalMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LookupOptionServiceImpl extends BaseService implements LookupOptionService {

    private LookupOptionRepository lookupOptionRepository;

    private SearchService searchService;

    @Autowired
    private void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    @Autowired
    private void setLookupOptionRepository(LookupOptionRepository lookupOptionRepository) {
        this.lookupOptionRepository = lookupOptionRepository;
    }

    @Override
    public LookupOption findByShortCode(String shortCode) throws RestException {
        return lookupOptionRepository.findByShortCode(shortCode).orElseThrow(() -> RestException.getRestException(RestException.TYPE_ERROR, Constants.LOOKUP_OPTION_DOES_NOT_EXIST));
    }

    @Override
    public PageDTO searchLookupOptions(SearchLookupOptionsDTO searchLookupOptionsDTO) throws RestException {
        LookupOptionSpecification specs = new LookupOptionSpecification(searchLookupOptionsDTO);

        Specification<LookupOption> lookupOptionSpec = specs.getSearchCriteria();

        Page<LookupOption> lookupOptionPage = lookupOptionRepository.findAll(lookupOptionSpec,
                PageRequest.of(searchLookupOptionsDTO.getPageNumber(), searchLookupOptionsDTO.getPageSize(),
                        Sort.by(Sort.Direction.ASC, "data")));

        PageDTO dto = searchService.getPage(lookupOptionPage, searchLookupOptionsDTO.getTransactionNumber());

        for (LookupOption lookupOption : lookupOptionPage.getContent()) {
            LookupDTO lookupDTO = lookupOption.toDTO();
            dto.getContent().add(lookupDTO);
        }

        return dto;
    }

    @Override
    public List<LookupDTO> getAllLookupOptionsByType(String type) throws RestException {
        List<LookupOption> lookupOptions = lookupOptionRepository.findAllByLookupType(type);
        List<LookupDTO> lookupDTOS = new ArrayList<>();
        if(!GlobalMethods.isListNullOrEmpty(lookupOptions)){
            lookupDTOS = lookupOptions.stream()
                    .map(LookupOption::toDTO)
                    .collect(Collectors.toList());
        }

        return lookupDTOS;
    }
}

