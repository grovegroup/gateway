package com.grove.mellow.services;

import com.grove.mellow.dto.lookup.LookupDTO;
import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.dto.search.SearchLookupOptionsDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.LookupOption;

import java.util.List;

public interface LookupOptionService {

    LookupOption findByShortCode(String shortCode) throws RestException;

    PageDTO searchLookupOptions(SearchLookupOptionsDTO searchLookupOptionsDTO) throws RestException;

    List<LookupDTO> getAllLookupOptionsByType(String type) throws RestException;
}
