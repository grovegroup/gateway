package com.grove.mellow.services;

import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.dto.search.PageDTO;
import com.grove.mellow.dto.tenantenvironment.TenantCreateDTO;
import com.grove.mellow.dto.tenantenvironment.TenantDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.Tenant;

import java.util.List;

public interface TenantService {

    void create(TenantCreateDTO tenantCreateDTO) throws RestException;

    void update(TenantDTO tenantDTO) throws RestException;

    void save(Tenant tenant) throws RestException;

    void updateStatus(String tenantId, boolean isDeleted) throws RestException;

    PageDTO search(BaseSearchDTO searchDTO) throws RestException;

    Tenant getById(String tenantId) throws RestException;

    Tenant getByTenantEnvironmentName(String tenantEnvironmentName) throws RestException;

    TenantDTO getTenantDTO(String tenantId) throws RestException;

    List<TenantDTO> getAllTenants() throws RestException;

    List<TenantDTO> getAllTenantsForApplicationServer() throws RestException;
}
