package com.grove.mellow.services;

import com.google.cloud.logging.Severity;

/**
 * Created by Barend on 2018/07/09.
 * Interface for all third party logging
 */
public interface LoggingService {

    /**
     * Writes to whichever logging system being used
     *
     * @param message
     * @param severity
     */
    void writeLog(String message, Severity severity);

}
