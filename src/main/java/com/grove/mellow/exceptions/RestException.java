package com.grove.mellow.exceptions;

import com.grove.mellow.utils.Constants;
import org.springframework.http.HttpStatus;

/**
 * Created by Barend on 2018/07/10.
 * The global rest exception used throughout the app
 */
public class RestException extends RuntimeException {

    /**
     * Class Constants
     */
    public static final int TYPE_SECURITY = 0;
    public static final int TYPE_ERROR_PROCESSING = 1;
    public static final int TYPE_ERROR = 2;
    public static final int TYPE_INVALID_REQUEST = 3;

    /**
     * Fields
     */
    private String errorType;
    private String message;
    private HttpStatus httpType;

    /**
     * Constructor
     *
     * @param errorType
     * @param message
     * @param httpType
     */
    public RestException(String errorType, String message, HttpStatus httpType) {
        this.errorType = errorType;
        this.message = message;
        this.httpType = httpType;
    }

    public RestException(String message) {
        super(message);
        this.message = message;
    }

    public RestException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    /**
     * Getters
     */
    @Override
    public String getMessage() {
        return message;
    }

    public String getErrorType() {
        return errorType;
    }

    public HttpStatus getHttpType() {
        return httpType;
    }


    /**
     * Creates the exception
     *
     * @param type
     * @param message
     * @return RestException
     */
    public static RestException getRestException(int type, String message) {
        switch (type) {
            case TYPE_ERROR_PROCESSING:
                return new RestException(Constants.JSON_RESPONSE_CODE_ERROR, message, HttpStatus.INTERNAL_SERVER_ERROR);
            case TYPE_SECURITY:
                return new RestException(Constants.JSON_RESPONSE_CODE_INVALID_TOKEN, message, HttpStatus.UNAUTHORIZED);
            case TYPE_INVALID_REQUEST:
                return new RestException(Constants.JSON_RESPONSE_CODE_BAD_DATA, message, HttpStatus.BAD_REQUEST);
            case TYPE_ERROR:
                return new RestException(Constants.JSON_RESPONSE_CODE_ERROR, message, HttpStatus.BAD_REQUEST);
        }
        return null;
    }

    /**
     * Basic Exception
     *
     * @return
     */
    public static RestException getRestGenericError() {
        return RestException.getRestException(RestException.TYPE_ERROR_PROCESSING, Constants.MESSAGE_GENERIC_ERROR);
    }

}
