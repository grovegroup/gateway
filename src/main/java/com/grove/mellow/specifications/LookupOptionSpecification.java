package com.grove.mellow.specifications;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.grove.mellow.dto.search.SearchLookupOptionsDTO;
import com.grove.mellow.models.LookupOption;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class LookupOptionSpecification {

    private SearchLookupOptionsDTO criteria;

    public LookupOptionSpecification(SearchLookupOptionsDTO criteria) {
        this.criteria = criteria;
    }

    public Specification<LookupOption> getSearchCriteria(){
        return new Specification<LookupOption>() {
            @Override
            public Predicate toPredicate(Root<LookupOption> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                Path<String> lookupType = root.get("lookupType");
                Path<String> data = root.get("data");
                Path<String> category = root.get("category");
                Path<Boolean> isDeleted = root.get("isDeleted");

                final List<Predicate> predicates = new ArrayList<Predicate>();

                if (!Strings.isNullOrEmpty(criteria.getLookupType())) {
                    predicates.add(cb.equal(lookupType, criteria.getLookupType()));
                }

                if (!Strings.isNullOrEmpty(criteria.getData())) {
                    predicates.add(cb.or(cb.like(cb.lower(data), "%" + criteria.getData().toLowerCase() + "%"), cb.like(cb.lower(category), "%" + criteria.getData().toLowerCase() + "%")));
                }

                predicates.add(cb.equal(isDeleted, false));

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }
}
