package com.grove.mellow.specifications;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.models.Tenant;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class TenantSpecification {

    private BaseSearchDTO criteria;

    public TenantSpecification(BaseSearchDTO criteria) {
        this.criteria = criteria;
    }

    public Specification<Tenant> getSearchCriteria(){
        return new Specification<Tenant>() {
            @Override
            public Predicate toPredicate(Root<Tenant> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                Path<String> tenantName = root.get("tenantName");

                final List<Predicate> predicates = new ArrayList<Predicate>();

                if(!Strings.isNullOrEmpty(criteria.getSearchText())){
                    predicates.add(cb.like(cb.lower(tenantName), "%" + criteria.getSearchText().toLowerCase() + "%"));
                }

                predicates.add(cb.notEqual(cb.lower(tenantName), "Default"));

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }
}



