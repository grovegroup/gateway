package com.grove.mellow.specifications;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.models.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class UserSpecification {

    private BaseSearchDTO criteria;

    public UserSpecification(BaseSearchDTO criteria) {
        this.criteria = criteria;
    }

    public Specification<User> getSearchCriteria(){
        return new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                Path<String> tenantId = root.get("tenantId");
                Path<Boolean> isDeleted = root.get("isDeleted");

                final List<Predicate> predicates = new ArrayList<Predicate>();
                predicates.add(cb.equal(isDeleted, false));

                if(!Strings.isNullOrEmpty(criteria.getSearchText())){
                    predicates.add(cb.equal(tenantId, criteria.getSearchText()));
                }

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }
}
