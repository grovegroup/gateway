package com.grove.mellow.controllers;

import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.dto.search.SearchUserDTO;
import com.grove.mellow.dto.user.UserRegisterDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.services.UserService;
import com.grove.mellow.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public class UserRestController extends BaseRestController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<?> createUser(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                          @RequestBody UserRegisterDTO userRegisterDTO) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("createUser");
            userService.create(userRegisterDTO);
            return ResponseSuccess("Success");
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("createUser");
        }
    }

    @PutMapping
    public ResponseEntity<?> updateUser(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                        @RequestBody UserRegisterDTO userRegisterDTO) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("updateUser");
            userService.update(userRegisterDTO);
            return ResponseSuccess("Success");
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("updateUser");
        }
    }

    @GetMapping("/me")
    public ResponseEntity<?> getMe(@RequestHeader(value = Constants.AUTH_HEADER) String authToken) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("getMe");
            return ResponseSuccess(userService.getMe());
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getMe");
        }
    }

    @GetMapping("/{userId}")
    public ResponseEntity<?> getUser(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                        @PathVariable String userId) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("getUser");
            return ResponseSuccess(userService.getUserProfileDTO(userId));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getUser");
        }
    }

    @GetMapping("/email/{emailAddress:.+}")
    public ResponseEntity<?> getUserByEmailAddress(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                     @PathVariable String emailAddress) {
        try {
            checkSecurityToken(authToken, true);
            startRestCall("getUserByEmailAddress");
            return ResponseSuccess(userService.getUserProfileByEmailAddress(emailAddress));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getUserByEmailAddress");
        }
    }

    @PutMapping("/status/{userId}/{status}")
    public ResponseEntity<?> updateStatus(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                          @PathVariable String userId,
                                          @PathVariable boolean status) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("updateStatus");
            userService.updateStatus(userId, status);
            return ResponseSuccess("Success");
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("updateStatus");
        }
    }

    @PutMapping("/search")
    public ResponseEntity<?> searchUsers(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                           @RequestBody SearchUserDTO searchDTO) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("searchUsers");
            return ResponseSuccess(userService.search(searchDTO));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("searchUsers");
        }
    }
}