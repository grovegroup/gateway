package com.grove.mellow.controllers;

import com.grove.mellow.dto.search.BaseSearchDTO;
import com.grove.mellow.dto.tenantenvironment.TenantCreateDTO;
import com.grove.mellow.dto.tenantenvironment.TenantDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.services.TenantService;
import com.grove.mellow.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/tenant")
public class TenantRestController extends BaseRestController {

    private TenantService tenantService;

    @Autowired
    public void setSecurityService(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    @PostMapping
    public ResponseEntity<?> createTenant(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                          @RequestBody TenantCreateDTO tenantCreateDTO) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("createTenant");
            tenantService.create(tenantCreateDTO);
            return ResponseSuccess("Success");
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("createTenant");
        }
    }

    @PutMapping
    public ResponseEntity<?> updateTenant(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                          @RequestBody TenantDTO tenantDTO) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("updateTenant");
            tenantService.update(tenantDTO);
            return ResponseSuccess("Success");
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("updateTenant");
        }
    }

    @GetMapping("/{tenantId}")
    public ResponseEntity<?> getTenant(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                          @PathVariable String tenantId) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("getTenant");
            return ResponseSuccess(tenantService.getById(tenantId));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getTenant");
        }
    }

    @PutMapping("/status/{tenantId}/{status}")
    public ResponseEntity<?> updateStatus(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                          @PathVariable String tenantId,
                                          @PathVariable boolean status) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("updateStatus");
            tenantService.updateStatus(tenantId, status);
            return ResponseSuccess("Success");
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("updateStatus");
        }
    }

    @PutMapping("/search")
    public ResponseEntity<?> searchTenants(@RequestHeader(value = Constants.AUTH_HEADER) String authToken,
                                          @RequestBody BaseSearchDTO searchDTO) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("searchTenants");
            return ResponseSuccess(tenantService.search(searchDTO));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("searchTenants");
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllTenants(@RequestHeader(value = Constants.AUTH_HEADER) String authToken) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("getAllTenants");
            return ResponseSuccess(tenantService.getAllTenants());
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getTenants");
        }
    }

    @GetMapping
    public ResponseEntity<?> getTenants(@RequestHeader(value = Constants.AUTH_HEADER) String authToken) {
        try {
            checkSecurityToken(authToken, true);
            startRestCall("getTenants");
            return ResponseSuccess(tenantService.getAllTenantsForApplicationServer());
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getTenants");
        }
    }
}
