package com.grove.mellow.controllers;

import com.grove.mellow.dto.login.LoginRequestDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.services.SecurityGroupService;
import com.grove.mellow.services.SecurityService;
import com.grove.mellow.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/securityGroup")
public class SecurityGroupRestController extends BaseRestController {

    private SecurityGroupService securityGroupService;

    @Autowired
    public void setSecurityGroupService(SecurityGroupService securityGroupService) {
        this.securityGroupService = securityGroupService;
    }

    @GetMapping
    public ResponseEntity<?> getSecurityGroups(@RequestHeader(value = Constants.AUTH_HEADER) String authToken) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("getSecurityGroups");
            return ResponseSuccess(securityGroupService.getSecurityGroups());
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getSecurityGroups");
        }
    }
}
