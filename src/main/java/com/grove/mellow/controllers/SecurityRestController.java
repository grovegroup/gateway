package com.grove.mellow.controllers;

import com.grove.mellow.dto.RegisterDTO;
import com.grove.mellow.dto.login.LoginRequestDTO;
import com.grove.mellow.dto.security.ResetPasswordDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.services.SecurityService;
import com.grove.mellow.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.apache.naming.ContextAccessController.checkSecurityToken;

/**
 * Created by Barend on 2018/07/12.
 * This is the rest controller for all user functionality
 */
@RestController
@RequestMapping("/api/v1/security")
public class SecurityRestController extends BaseRestController {

    private SecurityService securityService;

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(@RequestHeader(value = Constants.AUTH_HEADER) String authToken, @Valid @RequestBody LoginRequestDTO loginDTO) {
        try {
            checkSecurityToken(authToken, true);
            startRestCall("authenticateUser");
            if(authToken.equals(Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_GATEWAY)){
                return ResponseSuccess(securityService.gatewayLogin(loginDTO));
            }else{
                return ResponseSuccess(securityService.login(loginDTO));
            }
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("authenticateUser");
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestHeader(value = Constants.AUTH_HEADER) String authToken, @Valid @RequestBody RegisterDTO registerDTO) {
        try {
            checkSecurityToken(authToken, true);
            startRestCall("register");
            if(authToken.equals(Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_GATEWAY)){
                return ResponseSuccess(securityService.register(registerDTO));
            }else{
                throw RestException.getRestException(RestException.TYPE_SECURITY, Constants.MESSAGE_GENERIC_BAD_TOKEN);
            }
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("register");
        }
    }

    @PutMapping("/resetPassword")
    public ResponseEntity<?> resetPassword(@RequestHeader(value = Constants.AUTH_HEADER) String authToken, @RequestBody ResetPasswordDTO resetPasswordDTO) {
        try {
            checkSecurityToken(authToken, true);
            startRestCall("resetPassword");
            return ResponseSuccess(securityService.resetPassword(resetPasswordDTO));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("resetPassword");
        }
    }
}
