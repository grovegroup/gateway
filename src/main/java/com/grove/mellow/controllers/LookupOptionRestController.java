package com.grove.mellow.controllers;

import com.grove.mellow.dto.search.SearchLookupOptionsDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.services.LookupOptionService;
import com.grove.mellow.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/lookup")
public class LookupOptionRestController extends BaseRestController {

    private LookupOptionService lookupOptionService;

    @Autowired
    public void setLookupOptionService(LookupOptionService lookupOptionService) {
        this.lookupOptionService = lookupOptionService;
    }

    @PutMapping("/search")
    public ResponseEntity<?> searchLookupOptions(@RequestHeader(value = Constants.AUTH_HEADER) String authToken, @RequestBody SearchLookupOptionsDTO searchLookupOptionsDTO) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("searchLookupOptions");
            return ResponseSuccess(lookupOptionService.searchLookupOptions(searchLookupOptionsDTO));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("searchLookupOptions");
        }
    }

    @GetMapping("/{type}")
    public ResponseEntity<?> getAllLookupOptionsByType(@RequestHeader(value = Constants.AUTH_HEADER) String authToken, @PathVariable String type) {
        try {
            checkSecurityToken(authToken, false);
            startRestCall("getAllLookupOptionsByType");
            return ResponseSuccess(lookupOptionService.getAllLookupOptionsByType(type));
        } catch (RestException e) {
            e.printStackTrace();
            return HandleException(e);
        } finally {
            endRestCall("getAllLookupOptionsByType");
        }
    }
}
