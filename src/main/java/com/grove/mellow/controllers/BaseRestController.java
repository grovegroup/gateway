package com.grove.mellow.controllers;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.grove.mellow.dto.RestResponseDTO;
import com.grove.mellow.exceptions.RestException;
import com.grove.mellow.models.UserSession;
import com.grove.mellow.services.BaseService;
import com.grove.mellow.services.UserService;
import com.grove.mellow.services.UserSessionService;
import com.grove.mellow.utils.Constants;
import com.grove.mellow.utils.TransactionIdHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Barend on 2018/07/09.
 * The base rest controller.
 * All other rest controllers need to extend this class
 */
@RestController
public class BaseRestController extends BaseService {

    private UserService userService;

    private UserSessionService userSessionService;

    @Autowired
    private void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    private void setUserSessionService(UserSessionService userSessionService) {
        this.userSessionService = userSessionService;
    }

    /**
     * Returns the SUCCESS REST response to the client
     *
     * @param message
     * @param data
     * @param errors
     * @return
     */
    protected ResponseEntity<?> Response(String message, Object data, List<String> errors) {
        RestResponseDTO response = new RestResponseDTO(HttpStatus.BAD_REQUEST);
        response.setData(data);
        response.setErrors(errors);
        response.setTransactionId(TransactionIdHelper.get());
        response.setMessage(message);
        return response.Response();
    }

    /**
     * Generic Response
     *
     * @param message
     * @param data
     * @param error
     * @return
     */
    protected ResponseEntity<?> Response(String message, Object data, String error) {
        RestResponseDTO response = new RestResponseDTO(HttpStatus.BAD_REQUEST);
        response.setData(data);
        response.setErrors(Arrays.asList(error));
        response.setTransactionId(TransactionIdHelper.get());
        response.setMessage(message);
        return response.Response();
    }

    /**
     * Returns the SUCCESS REST response to the client
     *
     * @param data
     * @return
     */
    protected ResponseEntity<?> ResponseSuccess(Object data) {
        RestResponseDTO response = new RestResponseDTO(HttpStatus.OK);
        response.setData(data);
        response.setTransactionId(TransactionIdHelper.get());
        response.setMessage(RestResponseDTO.RESPONSE_SUCCESS);
        return response.Response();
    }

    /**
     * Returns the SUCCESS REST response to the client
     *
     * @param data
     * @param message
     * @return
     */
    protected ResponseEntity<?> ResponseSuccess(String message, Object data) {
        RestResponseDTO response = new RestResponseDTO(HttpStatus.OK);
        response.setData(data);
        response.setTransactionId(TransactionIdHelper.get());
        response.setMessage(message);
        return response.Response();
    }

    /**
     * Returns the FAIL REST response to the client
     *
     * @param message
     * @return
     */
    protected ResponseEntity<?> ResponseFail(RestException ex) {
        RestResponseDTO response = new RestResponseDTO(ex.getHttpType());
        response.setData("error");
        response.setTransactionId(TransactionIdHelper.get());
        response.setMessage(ex.getMessage());
        response.setErrors(Arrays.asList(ex.getMessage()));
        return response.Response();
    }

    /**
     * Handles the given error from the back-end - does any logging and handling that is needed
     *
     * @param ex
     * @return
     */
    protected ResponseEntity<?> HandleException(RestException ex) {
        ex.printStackTrace();
        return ResponseFail(ex);
    }

    /**
     * Indicates that a REST call has been successfully made
     *
     * @param name : Method name
     */
    public void startRestCall(String name) {
        logDebug(name + " - Start - " + TransactionIdHelper.get());
    }

    /**
     * Indicates that a REST call has successfully been completed
     *
     * @param name : Method name
     */
    public void endRestCall(String name) {
        logDebug(name + " - End - " + TransactionIdHelper.get());
    }

    protected void checkSecurityToken(String authToken, boolean beforeLogin) throws RestException {

        if (Strings.isNullOrEmpty(authToken)) {
            throw RestException.getRestException(RestException.TYPE_SECURITY, Constants.MESSAGE_GENERIC_BAD_TOKEN);
        }

        if (beforeLogin && (authToken.equals(Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_GATEWAY) || authToken.equals(Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION))) {
            return;
        }else if(beforeLogin && !authToken.equals(Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_GATEWAY) && !authToken.equals(Constants.SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION)){
            throw RestException.getRestException(RestException.TYPE_SECURITY, Constants.MESSAGE_GENERIC_BAD_TOKEN);
        }

        if (authToken.equals(httpSession.getAttribute(Constants.SECURITY_HTTP_SESSION_TOKEN_ATTR))) {
            userSessionService.updateUserSessionTokenExpiryTime(authToken);
            return;
        }

        UserSession userSession = userSessionService.getUserSessionByAuthToken(authToken);

        if (userSession != null) {
            userSessionService.updateUserSessionTokenExpiryTime(authToken);
            httpSession.setAttribute(Constants.SECURITY_HTTP_SESSION_TOKEN_ATTR, authToken);
            httpSession.setAttribute(Constants.SECURITY_HTTP_SESSION_USER_ATTR,  userService.findByEmailAddress(userSession.getEmailAddress()).get());
            return;
        }

        throw RestException.getRestException(RestException.TYPE_SECURITY, Constants.MESSAGE_GENERIC_BAD_TOKEN);
    }
}
