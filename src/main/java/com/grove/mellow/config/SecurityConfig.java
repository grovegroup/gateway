package com.grove.mellow.config;

import com.grove.mellow.security.RestAuthenticationEntryPoint;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

/**
 * Created by Barend on 2018/07/10.
 * Configuration for JWT Security
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {



    /**
     * Unauthorized Handler
     */
    private RestAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    public void setUnauthorizedHandler(RestAuthenticationEntryPoint unauthorizedHandler) {
        this.unauthorizedHandler = unauthorizedHandler;
    }

    /**
     * Config for JWT
     *
     * @param http : inherited
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/",
                        "/favicon.ico",
                        "/**/*.png",
                        "/**/*.gif",
                        "/**/*.svg",
                        "/**/*.jpg",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js")
                .permitAll()
                .antMatchers("/api/v1/security/**")
                .permitAll()
                .antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/security", "/swagger-ui.html", "/webjars/**") //Allows spring to pass
                .permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/users/**")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/tenant/**")
                .permitAll()
                .antMatchers("/api/v1/**")
                .permitAll()
                .anyRequest()
                .authenticated();
    }

    @Bean
    public SecurityConfiguration swaggerSecurityConfiguration() {
        return new SecurityConfiguration("client-id", "client-secret", "realm",
                "", "{{X-XSRF-COOKIE}}", ApiKeyVehicle.HEADER, "X-XSRF-TOKEN", ",");

    }

    @Bean
    public Docket api() throws IOException, URISyntaxException {
        final ApiInfo apiInfo = new ApiInfo(
                "Mellow REST API",
                "",
                "1.0",
                "",
                new Contact("Grove", "", "devsupport@groveis.com"),
                "",
                "",
                Collections.emptyList());
        return new Docket(DocumentationType.SWAGGER_2)
        .securitySchemes(Arrays.asList(new ApiKey("Access Token", AUTHORIZATION, In.HEADER.name())))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.grove.mellow.controllers"))
                .build()
                .apiInfo(apiInfo)
                .directModelSubstitute(Temporal.class, String.class);
    }
}
