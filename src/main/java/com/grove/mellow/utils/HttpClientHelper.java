package com.grove.mellow.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class HttpClientHelper {

    public static String httpPost(String urlString, Map<String,String> requestProperties, String jsonString){

        try{

            HttpURLConnection conn = init(urlString, Constants.HTTP_REQUEST_METHOD_POST);

            conn.setDoInput(true);

            for (Map.Entry<String, String> entry : requestProperties.entrySet()) {
                conn.setRequestProperty (entry.getKey(), entry.getValue());
            }

            try(OutputStream os = conn.getOutputStream()) {
                //"utf-8"
                byte[] input = jsonString.getBytes();
                os.write(input, 0, input.length);
            }

            return getHttpResponse(conn);
        }catch(IOException ioe){
            return null;
        }
    }

    public static String httpPut(String urlString, Map<String,String> requestProperties, String jsonString){

        try{

            HttpURLConnection conn = init(urlString, Constants.HTTP_REQUEST_METHOD_PUT);

            conn.setDoInput(true);

            for (Map.Entry<String, String> entry : requestProperties.entrySet()) {
                conn.setRequestProperty (entry.getKey(), entry.getValue());
            }

            try(OutputStream os = conn.getOutputStream()) {
                //"utf-8"
                byte[] input = jsonString.getBytes();
                os.write(input, 0, input.length);
            }

            return getHttpResponse(conn);
        }catch(IOException ioe){
            return null;
        }
    }

    public static String httpGet(String urlString, Map<String,String> requestProperties){

        try{

            HttpURLConnection conn = init(urlString, Constants.HTTP_REQUEST_METHOD_GET);

            for (Map.Entry<String, String> entry : requestProperties.entrySet()) {
                conn.setRequestProperty (entry.getKey(), entry.getValue());
            }

            return getHttpResponse(conn);
        }catch(IOException ioe){
            return null;
        }
    }

    private static HttpURLConnection init(String urlString, String requestMethod) throws IOException {

        try{
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod(requestMethod);
            conn.setUseCaches(false);
            conn.setDoOutput(true);

            return conn;
        }catch(IOException ex){
            throw ex;
        }
    }

    private static String getHttpResponse(HttpURLConnection conn) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        String result = "";
        String output;
        while ((output = br.readLine()) != null) {
            result = result + output;
        }

        return result;
    }
}
