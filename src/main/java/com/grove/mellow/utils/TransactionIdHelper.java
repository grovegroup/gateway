package com.grove.mellow.utils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Barend on 2018/07/10.
 */
public class TransactionIdHelper {

    /**
     * Atomic integer containing the next thread ID to be assigned
     */
    private static final AtomicInteger nextId = new AtomicInteger(0);

    /**
     * Thread local variable containing each thread's ID
     */
    private static final ThreadLocal<String> threadId =
            new ThreadLocal<String>() {
                @Override
                protected String initialValue() {
                    return GlobalMethods.generateUuid();
                }
            };

    /**
     * Returns the current thread's unique ID, assigning it if necessary
     *
     * @return
     */
    public static String get() {
        return threadId.get();
    }

}
