package com.grove.mellow.utils;

import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Barend on 2018/07/10.
 * This file that holds all the constant values of the API
 */
public interface Constants {

    /**
     * Request Properties
     */
    public static final String AUTH_HEADER = "Authorization";
    public static final String AUTH_BEARER = "Bearer ";
    public final static String JSON = "application/json";
    public final static String MULTIPART = "multipart/form-data";

    /**
     * Rest Messages
     */
    public static final String REST_UNAUTHORIZED = "Unauthorized";
    public static final String REST_USER_NOT_FOUND = "User not found with that username/email.";
    public static final String REST_USER_NOT_FOUND_WITH_UUID = "User not found with given credentials.";

    /**
     * Security
     */
    public final static String SECURITY_NON_LOGIN_AUTH_TOKEN_GATEWAY = "fleetControlGateway";
    public final static String SECURITY_NON_LOGIN_AUTH_TOKEN_APPLICATION = "fleetControl";
    public final static String SECURITY_HTTP_SESSION_TOKEN_ATTR = "SecurityToken";
    public final static String SECURITY_HTTP_SESSION_USER_ATTR = "User";
    public static final String MESSAGE_GENERIC_BAD_TOKEN = "Your session has expired";

    /**
     * Http
     */
    public static final String HTTP_REQUEST_METHOD_POST = "POST";
    public static final String HTTP_REQUEST_METHOD_PUT = "PUT";
    public static final String HTTP_REQUEST_METHOD_GET = "GET";

    /**
     * Error Codes
     */
    public static final String JSON_RESPONSE_CODE_BAD_DATA = "BAD_DATA";
    public static final String JSON_RESPONSE_CODE_INVALID_TOKEN = "INVALID_TOKEN";
    public static final String JSON_RESPONSE_CODE_ERROR = "ERROR";

    /**
     * Error Messages Error Processing
     */
    public static final String MESSAGE_GENERIC_ERROR = "An issue was encountered with your request - please try again";
    public static final String MESSAGE_GENERIC_SUCCESS = "Success";
    public static final String MESSAGE_GENERIC_BAD_DATA = "Invalid data was provided";

    /**
     * Security Group
     */
    public static final String ADMIN_SECURITY_GROUP_NAME = "ADMIN";
    public static final String OWNER_SECURITY_GROUP_NAME = "OWNER";
    public static final String BASIC_SECURITY_GROUP_NAME = "BASIC";
    public static final String SECURITY_ROLE_MISSING = "User role not provided.";
    public static final List<String> SECURITY_GROUPS_O = Arrays.asList(new String[]{OWNER_SECURITY_GROUP_NAME});
    public static final String SECURITY_GROUP_DOES_NOT_EXIST = "Security Group does not exist.";
    public static final String SECURITY_GROUP_ALREADY_EXISTS = "Security group with given name already exists.";

    /**
     * User
     */
    public static final String USER_REQUIRES_SECURITY_GROUP = "User requires at least one security group to be created.";
    public static final String USER_INVALID_FIRST_NAME = "Invalid first name provided for user.";
    public static final String USER_DOES_NOT_EXIST = "User does not exist.";
    public static final String USER_CREDENTIALS_DOES_NOT_EXIST = "User with this username/email address does not exist.";
    public static final String USER_ALREADY_EXISTS = "User with given username/email already exists.";
    public static final String USER_INVALID_USERNAME_OR_PASSWORD = "Invalid username/password provided. Please try again.";
    public static final String USER_NO_EMAIL_PROVIDED_CANT_RESET_PASSWORD = "No email address provided for requested user. Password can not be reset.";
    public static final String INVALID_TOKEN_RESET_PASSWORD = "Invalid token provided for reset password.";
    public static final String USER_INVALID_PASSWORD_PROVIDED = "Invalid password provided, please provide a valid password.";
    public static final String USER_INVALID_USERNAME_AND_EMAIL = "Please provide either your email or username to login.";

    /**
     * Tenant
     */
    public static final String TENANT_DOES_NOT_EXIST = "Tenant does not exist.";
    public static final String TENANT__MISSING = "Tenant is not provided.";
    public static final String TENANT_DISABLED = "Your licence has expired, please contact admin.";
    public static final String TENANT_NAME_EXISTS = "Tenant name already exists.";

    /**
     * Database Types
     */
    public static final String DATABASE_TYPE_MYSQL = "MySQL";
    public static final String DATABASE_TYPE_SQL = "SQL";
    public static final String DATABASE_TYPE_PGS = "Postgres";
    public static final String DATABASE_TYPE_MDB = "MongoDB";

    public static final String DATABASE_NAME_PLACEHOLDER = "<DATABASE_NAME>";

    /**
     * Feature
     */
    public static final String FEATURE_DOES_NOT_EXIST = "Feature does not exist.";

    /**
     * Menu
     */
    public static final String MENU_DOES_NOT_EXIST = "Menu does not exist.";

    /**
     * Menu Item
     */
    public static final String MENU_ITEM_DOES_NOT_EXIST = "Menu Item does not exist.";
    public static final String MENU_ITEM_RELATION = "Menu Item must belong to a Menu.";

    /**
     * LookupOption
     */
    public static final String LOOKUP_OPTION_DOES_NOT_EXIST = "The lookup option does not exist.";

    /**
     * Email
     */
    public final static String EMAIL_SUBJECT_RESET_PASSWORD = "Mellow - Reset Password";

    /**
     * Email Templates
     */
    public static final String RESET_PASSWORD_TEMPLATE = "<!DOCTYPE html><html><head></head><body><h3>Hi!</h3><p>A password reset has been requested for your account. If you requested this, please click the button bellow to reset your password.</p><p><a href='%s' class='es-button' target='_blank'>Reset Password</a></p></body></html>";
}
