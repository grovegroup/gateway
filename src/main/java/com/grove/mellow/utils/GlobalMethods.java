package com.grove.mellow.utils;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Barend on 2018/07/10.
 * Methods use throughout the application
 */
public class GlobalMethods {

    /**
     * Generates a new UUID
     *
     * @return String of new UUID
     */
    public static String generateUuid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    /**
     * Generates MD5 String
     * @param string to be converted to MD5 string
     * @return
     */
    public static String generateMD5(String string) {
        try {
            byte[] bytesOfMessage = string.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] theDigest = md.digest(bytesOfMessage);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < theDigest.length; ++i) {
                sb.append(Integer.toHexString((theDigest[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (Throwable t) {
            return "";
        }
    }

    public static Date addTimeToCurrentDate(int timeField, int value) {
        Calendar calendar = Calendar.getInstance();

        calendar.add(timeField, value);

        return calendar.getTime();
    }

    public static boolean isListNullOrEmpty(List<?> list){
        return (list == null || list.size() == 0);
    }
}
