package com.grove.mellow.utils;

public interface Preferences {

    public final static String TokenExpiryHours = "TokenExpiryHours";

    public final static String MySQLConnectionString = "MySQLConnectionString";
    public final static String MySQLDriverClassName = "MySQLDriverClassName";
    public final static String MySQLUserName = "MySQLUserName";
    public final static String MySQLPassword = "MySQLPassword";
}
