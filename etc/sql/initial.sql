insert into preference values ('MySQLConnectionString', now(), now(), false, 'B', 'jdbc:mysql://localhost:3306/<DATABASE_NAME>?verifyServerCertificate=false&useSSL=false');
insert into preference values ('MySQLDriverClassName', now(), now(), false, 'B', 'com.mysql.jdbc.Driver');
insert into preference values ('MySQLUserName', now(), now(), false, 'B', 'USERNAME');
insert into preference values ('MySQLPassword', now(), now(), false, 'B', 'PASSWORD');
insert into preference values ('TokenExpiryHours', now(), now(), false, 'B', '24');

insert into lookup_option values ('DB_MYSQL', now(), now(), false, null, 'MySQL', 'DATABASE_TYPE');

insert into tenant values (uuid(), now(), now(), false, 'jdbc:mysql://localhost:3306/fleetcontrol_default?verifyServerCertificate=false&useSSL=false', 'com.mysql.jdbc.Driver', 'PASSWORD', 'fleetcontrol_default', 'Default', 'USERNAME');

insert into security_group values (uuid(), now(), now(), false, 'OWNER', 'OWNER');
insert into security_group values (uuid(), now(), now(), false, 'BASIC', 'BASIC');
insert into security_group values (uuid(), now(), now(), false, 'ADMIN', 'ADMIN');

